/* ************* PRODUCTOS CRUD ************ */
function prod_validar_agregar(boton, formulario){  
    var url = "";
    var paso = 0;
    var bandera = true;
    
    if(boton == "btn_prod_paso1"){
        url = "/dashboard/productos/add_1/";
        paso = 1;
    }else if(boton == "btn_prod_paso2"){
        url = "/dashboard/productos/add_2/";
        paso = 2;
    }else if(boton == "btn_prod_paso3"){
        if($("#producto_fecha_inicio").val() != "" && $("#producto_fecha_fin").val() != "") {
            url = "/dashboard/productos/add_3/";
            paso = 3;
        }else{
            bandera = false;
            toastr.options = {
                "progressBar": true,
                "showDuration": "500",
                "hideDuration": "3000",
                "timeOut": "8000",
                "extendedTimeOut": "3000"
            };
            toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
        }
    }else if(boton == "btn_prod_paso4"){
        if($('#tags_producto').tagit("assignedTags") != "") {
            url = "/dashboard/productos/add_4/";
            paso = 4;
        }else{
            bandera = false;
            toastr.options = {
                "progressBar": true,
                "showDuration": "500",
                "hideDuration": "3000",
                "timeOut": "8000",
                "extendedTimeOut": "3000"
            };
            toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
        }

        //Si el numero de imagenes sobrepasa a (num ima actuales - 3)
        if((Number(document.getElementById("id_imagenes_producto").files.length) > Number($("#numero_imagenes_actuales").val())) && Number($("#numero_imagenes_actuales").val()) != 0){
            toastr.warning("Solo pude subir hasta "+$("#numero_imagenes_actuales").val()+" imágen/es", 'Advertencia');
            bandera = false;
        }
    }
    if(bandera == true)
        prod_agregar_producto(url, formulario, paso)
}

function prod_agregar_producto(url, formulario, paso){
    var form = document.forms.namedItem(formulario);
    datos = new FormData(form);
    datos.append('id_producto', $("#id_producto").val());
    
    if(paso == 4){ datos.append('tags_producto', $('#tags_producto').tagit("assignedTags")); }
    
    crear_alerta_gif();
    
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        async: true,
        data: datos,
        processData: false,
        contentType: false,
        success: function (json) {
            swal.close();
            if (json.result == "OK") {
                toastr.options = {
                    "progressBar": true,
                    "showDuration": "500",
                    "hideDuration": "3000",
                    "timeOut": "8000",
                    "extendedTimeOut": "3000"
                };
                toastr.success(json.message, 'Correcto');
                $("#id_producto").val(json.id);
                if(paso == 3){$("#producto_id_oferta").val(json.id_oferta);}
                if(paso == 4){
                    $("#producto_id_imagen").val(json.id_imagen);
                    if(json.bandera_imagen == 1) $("#id_restablecer_imagenes").remove()
                }

                //Cambios que pudieran presentarse al modificar los campos (de los 4 pasos)
                comprobar_cambios(paso);

                //Activar el siguiente paso
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                $($active).next().find('a[data-toggle="tab"]').click();
            } else {
                toastr.options = {
                    "progressBar": true,
                    "showDuration": "500",
                    "hideDuration": "3000",
                    "timeOut": "8000",
                    "extendedTimeOut": "3000"
                };
                toastr.error(json.message, 'Estado');
            }                
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

/* ************* PRODUCTOS EN CUARENTENA CRUD ************ */
function prod_agregar_cuarentena(url){
    if($("#formulario_producto_cuarentena").valid() == true) {
        var form = document.forms.namedItem("formulario_producto_cuarentena");
        datos = new FormData(form);
        datos.append('cuarentena_id', $("#menu_productos_cuarentena").data("test").cuarentena_id);
        datos.append('producto_editar_id', $("#menu_productos_cuarentena").data("test").producto_editar_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_productos_cuarentena').DataTable();
                    var ii = "('/dashboard/productos/cuarentena/cargar?id="+json.id+"', this)";
                    var ij = "('/dashboard/productos/cuarentena/restaurar?id="+json.id+"', this)";
                    botones = '<div class="btn-group"><button onclick="prod_cargar_cuarentena'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button href="#" onclick="prod_restaurar_cuarentena'+ij+'" class="btn btn-success btn-xs" title="Restaurar" ><i class="fa fa-exchange"></i></button></div>';

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#menu_productos_cuarentena").data("test").cuarentena_id != ""){
                        oculto_this = $("#menu_productos_cuarentena").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }
                    dTable.row.add([
                        json.nombre_producto,
                        json.categoria,
                        json.cantidad,
                        json.nota,
                        botones
                    ]).draw(false);
                    
                    $("#modal_productos_cuarentena").modal('hide');

                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function prod_cargar_cuarentena(url, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                $("#id_producto_cuarentena").val(json.producto_id);
                $("#cuarentena_cantidad").val(json.cantidad);
                $("#cuarentena_nota").val(json.nota);
                $("#menu_productos_cuarentena").data("test", {cuarentena_id: json.id, boton_this: boton, producto_editar_id: json.producto_id});

                $("#id_producto_cuarentena").select2({
                    selected: json.producto_id,
                    disabled: true
                });

                $("#modal_productos_cuarentena").modal('show');
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

function prod_restaurar_cuarentena(url, boton) {
    swal({
            title: "Confirmación de restauración",
            text: "¿Está seguro que desea restaurar el stock de estos productos?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonColor: "#3c8dbc ",
            confirmButtonText: "Si, restaurar stock",
            cancelButtonText: "No, cancelar",
            showLoaderOnConfirm: true
        },
        function() {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',

            success: function (json) {
                if (json.result == "OK") {
                    swal("¡Correcto!", json.message, "success");

                    var dTable = $("#tabla_productos_cuarentena").DataTable();
                    dTable.row($(boton).closest('tr')).remove().draw(false);
                } else {
                    swal("Ha ocurrio un error", json.message, "error");
                }
            },
            error: function (e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    });
}

function abrir_modal_cuarentena() {
    reset_formulario("formulario_producto_cuarentena", "");
    $("#id_producto_cuarentena").select2({disabled: false});
    $("#modal_productos_cuarentena").modal('show');
    $("#menu_productos_cuarentena").data("test", {cuarentena_id: "", producto_editar_id: ""});
}


/* ************* REVIEWS CRUD ************ */


/* ************* IMAGENES CRUD ************ */
function prod_cambiar_img_principal(url, boton) {
    swal({
            title: "Confirmación de cambios",
            text: "¿Está seguro que desea cambiar la imagen principal de este producto?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonColor: "#67809F",
            confirmButtonText: "Si, cambiar",
            cancelButtonText: "No, cancelar",
            showLoaderOnConfirm: true
        },
        function() {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',

            success: function (json) {
                if (json.result == "OK") {
                    swal("¡Correcto!", json.message, "success");
                    $("#img_es_principal"+json.id_antiguo).text("");
                    var eli = "('/dashboard/productos/imagenes/cambiar?id="+json.id_antiguo+"', this)";
                    $("#img_es_principal"+json.id_antiguo).closest('tr').find(".btn-group").append('<button onclick="prod_cambiar_img_principal'+eli+'" class="btn label-success btn-xs"></i> Cambiar</button>');

                    $("#img_es_principal"+json.id_nuevo).text("Imagen principal");
                    $(boton).remove();
                } else {
                    swal(json.message, "", "info");
                }
            },
            error: function (e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    });
}

function abrir_modal_subir_imagen(id, boton) {
    $("#id_subir_imagen").fileinput('clear');
    $("#modal_cambiar_imagen").modal('show');
    $("#modal_cambiar_imagen").data("test", {imagen_subir_id: id, boton_this: boton});
}

function prod_subir_imagen(url) {
    if($("#id_subir_imagen").val() != "") {
        var form = document.forms.namedItem("formulario_cambiar_imagen");
        datos = new FormData(form);
        datos.append('imagen_subir_id', $("#modal_cambiar_imagen").data("test").imagen_subir_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');
                    
                    boton = $("#modal_cambiar_imagen").data("test").boton_this;
                    $(boton).closest('tr').find("img").attr("src", json.url);

                    $("#modal_cambiar_imagen").modal('hide');
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Error');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.warning("Debe elegir una imagen", "¡Advertencia!");
    }
}
/* ************* OFERTAS CRUD ************ */
function prod_agregar_ofertas(url){
    if($("#formulario_crud_ofertas").valid() == true) {
        var form = document.forms.namedItem("formulario_crud_ofertas");
        datos = new FormData(form);
        datos.append('oferta_id', $("#modal_crud_ofertas").data("test").oferta_id);
        datos.append('producto_oferta_id', $("#modal_crud_ofertas").data("test").producto_oferta_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_ofertas').DataTable();
                    var ii = "('/dashboard/productos/ofertas/cargar?id="+json.id+"', this)";
                    var ij = "('/dashboard/productos/ofertas/eliminar?id="+json.id+"', 'OF', 'tabla_ofertas', this)";
                    botones = '<div class="btn-group"><button onclick="abrir_modal_ofertas'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button href="#" onclick="prod_eliminar_objetos'+ij+'" class="btn btn-danger btn-xs" title="Eliminar" ><i class="fa fa-trash-o"></i></button></div>';

                    producto = '<span class="label label-success">ID: '+json.id_producto+'</span>  '+json.nombre_producto;

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#modal_crud_ofertas").data("test").oferta_id != ""){
                        oculto_this = $("#modal_crud_ofertas").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }
                    fecha_inicio = convertir_fecha_larga(json.fecha_i);
                    fecha_fin = convertir_fecha_larga(json.fecha_f);

                    dTable.row.add([
                        fecha_inicio,
                        fecha_fin,
                        json.porcentaje +"%",
                        producto,
                        botones
                    ]).draw(false);

                    $("#modal_crud_ofertas").data("test", {oferta_id: "", producto_oferta_id: ""});
                    $("#modal_crud_ofertas").modal('hide');
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function cargar_productos_select(url, texto, id){
     $.ajax({
        url:url,
        dataType: "json",
        async: true,
        success: function(json){
            $("#"+id).select2({
                placeholder: texto,
                data: json,
                allowClear: true
            });
        }
    });
}
function abrir_modal_ofertas(url, boton) {
    //Si es nuevo, cargar el combo de productos
    $("#div_oferta_prod_crud").remove();
    reset_formulario("formulario_crud_ofertas", "");

    if(url == "0"){
        $('<div id="div_oferta_prod_crud" class="form-group"><label>Producto</label><select id="id_producto_oferta_crud" name="id_producto_oferta_crud" style="width: 99%;"></select><div>').insertBefore(".box-body .form-group:first");
        cargar_productos_select("/dashboard/consulta/productos/", "Elija un producto", "id_producto_oferta_crud");
    }else{
        prod_cargar_ofertas(url, boton)
    }
    $("#modal_crud_ofertas").modal('show');
}

function prod_cargar_ofertas(url, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            if (json.result == "OK"){
                $("#producto_fecha_inicio_crud").datepicker('setDate', new Date(json.fecha_i));
                $("#producto_fecha_fin_crud").datepicker('setDate', new Date(json.fecha_f));
                $("#producto_precio_oferta_crud").val(json.porcentaje);
                $("#modal_crud_ofertas").data("test", {oferta_id: json.id, producto_oferta_id: json.producto_id, boton_this: boton});
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
            swal.close();
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}


/* ************* TAGS CRUD ************ */
function prod_agregar_tag(url){
    var band_agregar = true;
    var band_editar= true;

    if($('#nombre_tag').tagit("assignedTags").length == 0 && $("#nombre_tag_editar").val() == null)
        band_agregar = false;

    if($("#nombre_tag_editar").val() == "")
        band_editar = false;


    if(band_agregar == true && band_editar == true){
        var form = document.forms.namedItem("formulario_producto_tag");
        datos = new FormData(form);
        datos.append('nombre_tag', $('#nombre_tag').tagit("assignedTags"));
        datos.append('id_tag_editar', $("#menu_productos_tags_administrar").data("test").tag_id);

        datos.append('nombre_tag_editar', $("#nombre_tag_editar").val());

        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');
                    var dTable = $('#tabla_tags').DataTable();
                    
                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if(json.bandera == "E"){
                        $('#nombre_tag').show();
                        $("#menu_productos_tags_administrar").data("test", {tag_id: ""});
                        $('#nombre_tag_editar').remove();
                    }
                    $('#tabla_tags').DataTable().rows('tr').remove().draw(false);

                    for(var key in json.todos){
                        var ii = "('/dashboard/productos/tags/cargar?id="+json.todos[key].id+"', this)";
                        var ij = "('/dashboard/productos/tags/eliminar?id="+json.todos[key].id+"','TA', 'tabla_tags', this)";
                        botones = '<div class="btn-group"><button onclick="prod_cargar_tags'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button onclick="prod_eliminar_objetos'+ij+'" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"</i></button></div>';

                        dTable.row.add([
                            Number(key)+1,
                            json.todos[key].nombre,
                            botones
                        ]).draw(false);
                    }

                    $('#nombre_tag').tagit("removeAll");
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
                swal.close();
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("El campo no debe estar vacío", 'Estado');
    }
}

function prod_cargar_tags(url, boton){
    crear_alerta_gif();
    $('#nombre_tag_editar').remove();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                $('#nombre_tag').hide();
                $('.form-group').find('span:first').append("<input  autofocus class='form-control' id='nombre_tag_editar' maxlength='20' value='"+json.nombre+"' type='text' />");
                $("#menu_productos_tags_administrar").data("test", {tag_id: json.id, boton_this: boton});
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

/* *******************************  FUNCIONES VARIAS ************************************* */
function abrir_modal_producto(id, url){
    $("#modal_detalle_producto").load(url+"?id="+id+" #modal_detalle_producto>*","");
}

function listo_next_step(boton, formulario){
    if($("#"+formulario).valid() == true){
        prod_validar_agregar(boton, formulario);        
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function saltar_next_step() {
    var $active = $('.wizard .nav-tabs li.active');
    $active.next().removeClass('disabled');
    $($active).next().find('a[data-toggle="tab"]').click();
}

function comprobar_cambios(paso) {
    //Actualizar notificaciones cuando se cambie el stock de los productos
    if(paso == 2){
        if($("#stock_producto").closest('.form-group').hasClass('has-warning') == true){
            if(Number($("#stock_producto").val()) >= Number($("#stock_min_producto").val())){
                $("#stock_producto").closest('.form-group').removeClass('has-warning');
                $("#span_bajo_stock").hide();
                actualizar_panel_notificaciones();
            }else{
                toastr.info("El producto se encuentra por debajo del stock mínimo", 'Información');
            }
        }else{
            if(Number($("#stock_producto").val()) <= Number($("#stock_min_producto").val())){
                toastr.info("El producto se encuentra por debajo del stock mínimo", 'Información');
                $("#stock_producto").closest('.form-group').addClass('has-warning');
                actualizar_panel_notificaciones();
            }
        }
    }else if(paso == 4){
        $("#div_producto_destacado").load(location.href+" #div_producto_destacado>*","");
    }
}

function filtrar_nombre_productos(ruta, valor, tabla) {
    var url = ruta+"?id="+$("#"+valor).val();
    crear_alerta_gif();
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',

        success: function(json){
            swal.close();
            $('#'+tabla+'_filter label input[type=search]').val(json.nombre).trigger($.Event("keyup", { keyCode: 13 }));
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}