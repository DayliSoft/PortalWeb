$(function() {
    $('#modalIngresoDeposito').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('id');
        var num_deposito = $(e.relatedTarget).data('codigo-deposito');
        var imagen_deposito = $(e.relatedTarget).data('imagen-deposito');

        $("#contenedor_img").append('<center><img style="width: 300px;height: 300px;" src="'+imagen_deposito+'"  alt="" /></center>');
        if (num_deposito!= 'None') {
            $("#txt_deposito").val(num_deposito);
        }else{
            $("#txt_deposito").val('');
        }
        $("#txt_id").val(id);
    });

    $('#modalIngresoDeposito').on('hidden.bs.modal', function(e) {
        $("#contenedor_img").empty();
        $("#txt_deposito").val('');
        $("#txt_id").val('');
    });

    $('#form_deposito').on("submit", function() {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/shop/ingresar_deposito/",
            data: new FormData($('#form_deposito')[0]),
            processData: false,
            contentType: false,
            success: function(response) {
                response = JSON.parse(response);
                console.log(response);
                $('#modalIngresoDeposito').modal('hide');
                if (response.result === 'OK') {
                    swal({
                        title: "Muy bien",
                        text: "Su deposito a sido registrado con exito, despues de su verificación un asistente se comunicara con usted para confirmarle el envio.",
                        type: "info",
                        closeOnConfirm: true
                    },
                    function(){
                        window.location.reload();
                    });
                }
            },
            error: function() {
                console.log('Error');
            }
        });
    });
    function archivo(evt) {

        var files = evt.target.files; // FileList object

        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
        //Solo admitimos imágenes.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        reader.onload = (function(theFile) {
            return function(e) {
              // Insertamos la imagen
             document.getElementById("contenedor_img").innerHTML = ['<center><img style="width: 300px;height: 300px;" src="'+ e.target.result+'"  alt="" /></center>'].join('');
             
            };
        })(f);

        reader.readAsDataURL(f);
        }
    }
 
    document.getElementById('file1').addEventListener('change', archivo, false);
});