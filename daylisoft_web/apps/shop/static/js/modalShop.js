$('#modalIngresoCarrito').on('show.bs.modal', function(e) {
    var precioMaster = 0.0;
    var id = $(e.relatedTarget).data('id');
    var nombre = $(e.relatedTarget).data('nombre');
    var imagen = $(e.relatedTarget).data('img');
    var cantidad = $(e.relatedTarget).data('cantidad');
    var precio = $(e.relatedTarget).data('precio');
    var precioOferta = $(e.relatedTarget).data('preoferta');
    var oferta = $(e.relatedTarget).data('oferta');
    $("#prod_titulo").append(nombre);
    if(oferta==="True"){
        $("#prod_precio").append('$'+precioOferta);
        precioMaster = precioOferta; 
    }else{
        $("#prod_precio").append('$'+precio);
        precioMaster = precio;
    }
    $("#prod_cantidad").append(cantidad);
    $("#prod_imagen").append('<img class="img-responsive" id="largeImage" src="'+imagen+'"  alt="" />')
    $("#contenedorBotones").append('<div class="col-sm-4 col-xs-6"><a href="/shop/producto_detalle/'+id+'" class="shortcode_button btn_small btn_type5 cart_btn"><i class="icon-puzzle-piece"></i>detalle</a></div>');
    $("#contenedorBotones").append('<div class="col-sm-offset-4 col-sm-4 col-xs-6"><a href="javascript:void(0);" onClick=\'AddCarModal('+id+',"'+precioMaster+'","'+nombre+'","'+imagen+'")\' class="shortcode_button btn_small btn_type5 cart_btn" data-dismiss="modal"><i class="icon-shopping-cart"></i>agregar</a></div>')
    $("#botonesMas_Menos").append('<a class="minus button boton" onClick="sumar_restar(this)" href="javascript:void(0);">-</a>');
    $("#botonesMas_Menos").append('<input id="txt_cantidad" class="input-text qty text" type="number" title="Qty" value="1" name="quantity" min="1" step="1" disabled>');
    $("#botonesMas_Menos").append('<a class="plus button boton" onClick="sumar_restar(this)" href="javascript:void(0);">+</a>');
});

$('#modalIngresoCarrito').on('hidden.bs.modal', function(e) {
    $("#prod_titulo").empty();
    $("#prod_precio").empty();
    $("#prod_cantidad").empty();
    $("#prod_imagen").empty();
    $("#contenedorBotones").empty();
    $("#botonesMas_Menos").empty();
    $("#txt_cantidad1").attr('value','1');
});


function sumar_restar(boton){

    var button = $(boton);
    var oldValue = $("#txt_cantidad").val();
    var cant_max = $("#prod_cantidad").text();

    if (button.text() == "+") {
        if(oldValue < parseFloat(cant_max)){
            var newVal = parseFloat(oldValue) + 1;
        }else{
            var newVal = parseFloat(oldValue);
        }
    } else {
        if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 1;
        }
    }
    $("#txt_cantidad").val(newVal);
    $("#txt_cantidad1").attr('value',newVal);
}