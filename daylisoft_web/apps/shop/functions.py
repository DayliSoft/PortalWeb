# -*- coding: utf-8 -*-
from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
from .models import *
from datetime import time, date
from apps.daylisoft_admin.models import perfil_empresa
from apps.daylisoft_admin.models import impuesto
from django.template.loader import render_to_string
from django.core.mail import send_mail
from decimal import *
from twilio.rest import TwilioRestClient
#from twilio import TwilioRestException

'''PARAMETROS:
request: Request de la vista
modelo: Modelo a utilizar en la paginación
paginas: Cantidad de paginas del paginador
nonRepPag: Identificador del result list
'''
def Paginador(request, modelo, paginas, nonRegPag):

    #Retorna el objeto paginator para comenzar el trabajo
    result_list = Paginator(modelo, paginas)
    try:
     #Tomamos el valor de parametro page, usando GET
     page = int(request.GET.get('page'));
    except:
     page = 1

    #Si es menor o igual a 0 igualo en 1
    if page <= 0:
        page = 1

    #Si el parámetro es mayor a la cantidad
    #de paginas le igualo el parámetro con las cant de paginas
    if(page > result_list.num_pages):
        page = result_list.num_pages

    #Verificamos si esta dentro del rango
    if (result_list.num_pages >= page):
        #Obtengo el listado correspondiente al page
        pagina = result_list.page(page)
        Contexto = {nonRegPag: pagina.object_list, #Asignamos registros de la pagina
                   'page': page, #Pagina Actual
                   'pages': result_list.num_pages, #Cantidad de Paginas
                   'has_next': pagina.has_next(), #True si hay proxima pagina
                   'has_prev': pagina.has_previous(), #true si hay Pagina anterior
                   'next_page': page+1, #Proxima pagina
                   'prev_page': page-1, #Pagina Anterior
                   'firstPage': 1,
                   }
        return Contexto

def calcular_valoracion_producto(producto,data):
    try:
        acumulador = 0.0
        for val in data:
            acumulador = acumulador + val.rev_estrellas
        promedio = acumulador / len(data)
        redondeado = int(round(promedio))
        producto.pro_valoracion = redondeado
        producto.save()
    except Exception as e:
        redondeado = 5
        print e
    return redondeado

def pro_relacionados(prod):
    productos = []
    try:
        find_tags = prod.tags.all()
        find_prod_related = producto.objects.filter(tags = find_tags).exclude(pro_id=prod.pk)[:3]
        if find_prod_related.exists():
            productos = buscar_oferta(find_prod_related)
    except Exception as e:
        print e
    return productos

def pro_mas_destacados():
    productos = []
    try:
        find_prod = producto.objects.filter(pro_destacado=True).exclude(pro_estado=False).order_by("-pro_id")[:3]
        if find_prod.exists():
            productos = buscar_oferta(find_prod)
    except Exception as e:
        print e
    return productos

def pro_mas_puntuados():
    productos = []
    try:
        find_prod = producto.objects.all().exclude(pro_estado=False).order_by("-pro_valoracion")[:3]
        if find_prod.exists():
            productos = buscar_oferta(find_prod)
    except Exception as e:
        print e
    return productos

def buscar_oferta(lista_prod):
    productos = []
    time = date.today()
    for prod in lista_prod:
        find_oferta = oferta.objects.filter(pro_id = prod,
            ofer_fecha_inicio__lte=time, ofer_fecha_fin__gte=time)
        find_img = imagen_producto.objects.get(pro_id = prod, img_es_principal=True)
        if find_oferta.exists():
            pre_oferta = prod.pro_precio - (prod.pro_precio * (find_oferta[0].ofer_porcentaje / 100))
            productos.append({
                'producto' : prod,
                'oferta' : True,
                'pre_oferta' : format(pre_oferta, '.2f'),
                'imagen' : find_img.img_data,
                })
        else:
            productos.append({
                'producto' : prod,
                'imagen' : find_img.img_data,
                })
    return productos

# Valida si la cantidad del producto existen en la bd
def validar_producto(find_prod,cantidad):
    if int(cantidad) <= int(find_prod.pro_stock):
        dic = {
            "result" : "OK"
        }
    else:
        dic = {
            "result" : "PRODUCTO",
            "mensaje" : "El producto "+find_prod.pro_nombre+" no cuenta con la cantidad de "+str(cantidad)+" unidades disponibles por el momento."
        }
    return dic

def nuevo_pedido_notificacion(instance):
    try:
        envio_email(instance)
        envio_sms(instance)
    except Exception as e:
        raise e

def envio_email(instance):
    print 'Enviando email'
    productos = []
    subtotal = 0
    costo_envio = 0
    try:
        find_empresa = perfil_empresa.objects.get(per_ruc='0791786371001')
        find_det_pedido = det_pedido.objects.filter(ped_id=instance)
        find_iva = impuesto.objects.get(imp_actual=True)
        if instance.trans_id != None:
            costo_envio = instance.trans_id.trans_costo
        else:
            costo_envio = 0
        if find_det_pedido.exists():
            for det_ped in find_det_pedido:
                find_img = imagen_producto.objects.get(pro_id=det_ped.pro_id, img_es_principal=True)
                productos.append({
                    'prod' : det_ped,
                    'img' : find_img.img_data.url
                    })
                subtotal = subtotal + det_ped.total_pagar

            ctx = {
                'productos': productos,
                'datos_empresa': find_empresa,
                'pedido': instance,
                "iva" : find_iva.imp_porcentaje,
                'subtotal': subtotal,
                'valor_iva': round(subtotal * (Decimal(find_iva.imp_porcentaje)/100), 2),
                'envio': costo_envio
            }

            txt_email = instance.cliente.email #Hay que enviarle al correo del cliente
            asunto = 'Nuevo pedido ID: #'+str(instance.pk)
            msg_plain = render_to_string('template_emails/nuevo_pedido/index.txt', ctx)
            msg_html = render_to_string('template_emails/nuevo_pedido/index.html', ctx)

            send_mail(
                asunto,
                msg_plain,
                'notificaciones.daylisoft@gmail.com',
                [txt_email],
                html_message=msg_html,
            )

            # notificacion a la empresa
            ctx2 = {
                'valor': instance.ped_total_pagar,
                'usuario' : instance.cliente
            }

            asunto = 'Nuevo pedido ID: #'+str(instance.pk)
            msg_plain = render_to_string('template_emails/notificacion_empresa/index.txt', ctx2)
            msg_html = render_to_string('template_emails/notificacion_empresa/index.html', ctx2)

            send_mail(
                asunto,
                msg_plain,
                'notificaciones.daylisoft@gmail.com',
                [find_empresa.per_email],
                html_message=msg_html,
            )
        else:
            print 'no existe ningun producto creado en el pedido'

    except Exception as e:
        print 'No se pudo enviar el mail'
        raise e

def envio_sms(instance):
    print 'Enviando un mensaje de texto'
    try:
        account_sid = "AC743b01dd61b556411c8cf84d2254a81d"
        auth_token  = "bfae3d305821bf7809d84ca7c1633bf3"
        client = TwilioRestClient(account_sid, auth_token)

        message = client.messages.create(body="Hola se acaba de registrar un pedido por un valor de $"+instance.ped_total_pagar+" dolares",
            to="+593980380351",    # Replace with your phone number
            from_="+12059906675") # Replace with your Twilio number

        print(message.sid)
    except Exception as e:
        print e
        # raise e
