# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404, render_to_response, RequestContext
from .models import *
from .functions import *
from datetime import datetime, date, time, timedelta
from apps.users.models import *
from django.http import JsonResponse,HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db import transaction
import json
from apps.daylisoft_admin.models import impuesto
from decimal import *
from django.db.models import Q
# Create your views here.

def productos_generales(request,clave_valor):
    print clave_valor
    productos = []
    clave = clave_valor.split("=")
    txt_search = {
        "id_buscado":clave[1]
    }
    if clave[0]=='q':
        if clave[1]=='all':
            find_productos = producto.objects.all().exclude(pro_estado=False).order_by("-pro_id")
            txt_search["tipo"] = 'all'
        else:
            find_productos = producto.objects.filter(Q(pro_nombre__icontains=clave[1]) | 
                Q(pro_descripcion_corta__icontains=clave[1]) | 
                Q(pro_descripcion_larga__icontains=clave[1])).order_by("-pro_id")
    elif clave[0]=='cant':
        rangos = clave[1].split('-')
        find_productos = producto.objects.filter(pro_precio__gte=rangos[0],pro_precio__lte=rangos[1]).exclude(pro_estado=False).order_by("-pro_id")
        txt_search["tipo"] = 'cantidad'
        txt_search["min"] = rangos[0]
        txt_search["max"] = rangos[1]
    elif clave[0]=='cat':
        find_productos = producto.objects.filter(categorias__cat_nombre__icontains=clave[1]).exclude(pro_estado=False).order_by("-pro_id")
        txt_search["tipo"] = 'cat'
    elif clave[0]=='tag':
        find_productos = producto.objects.filter(tags__tag_nombre__icontains=clave[1]).exclude(pro_estado=False).order_by("-pro_id")
        txt_search["tipo"] = 'tag'
    else:
        find_productos = producto.objects.all().exclude(pro_estado=False).order_by("-pro_id")
        txt_search["tipo"] = 'all'

    #Inicio el paginador
    pag = Paginador(request, find_productos, 12, 'productos')
    time = date.today()
    for find_prod in pag['productos']:
        img = imagen_producto.objects.get(pro_id = find_prod, img_es_principal=True)
        find_oferta = oferta.objects.filter(pro_id = find_prod, ofer_fecha_inicio__lte=time, ofer_fecha_fin__gte=time)
        if find_oferta.exists():
            pre_oferta = find_prod.pro_precio - (find_prod.pro_precio * (find_oferta[0].ofer_porcentaje / 100))
            productos.append({
                'prod' : find_prod,
                'img' : img.img_data.url,
                'pre_oferta': format(pre_oferta, '.2f'),#nueva forma de formatear wtf
                'oferta': True
            })
        else:
            productos.append({
                'prod' : find_prod,
                'img' : img.img_data.url,
                'oferta': False
            })

    print txt_search
    #Contexto a retornar a la vista
    cxt = {
    'productos': productos,
    'totPost': find_productos,
    'paginator': pag,
    'categorias': categoria.objects.all(),
    'tags' : tag.objects.all(),
    'productos_destacados' : pro_mas_destacados(),
    'productos_puntuados' : pro_mas_puntuados(),
    'respuesta' : txt_search,
    }
    return render_to_response('shop/shop.html', context_instance=RequestContext(request, cxt))

def redirec_prod(request):
    return redirect('/shop/productos/q=all/')

def producto_detalle(request, id):
    try:
        time = date.today()
        find_prod = producto.objects.get(pro_id = id)
        find_imagenes = imagen_producto.objects.filter(pro_id = find_prod)
        find_oferta = oferta.objects.filter(pro_id = find_prod, ofer_fecha_inicio__lte=time, ofer_fecha_fin__gte=time)
        find_comentarios = review.objects.filter(rev_producto = find_prod)
        if find_comentarios.exists():
            valoracion = calcular_valoracion_producto(find_prod,find_comentarios)
        else:
            valoracion = 5
        dic = {
                'producto' : find_prod,
                'imagenes' : find_imagenes,
                'comentarios': find_comentarios,
                'cant_coment' : len(find_comentarios),
                'oferta': False,
                'categorias': categoria.objects.all(),
                'tags' : tag.objects.all(),
                'valoracion' : valoracion,
            }
        # gestion si existe la oferta (validar que solo pueda ingresarse una oferta en una rango de fechas que no afecte a las demas en ese producto)
        if find_oferta.exists():
            pre_oferta = find_prod.pro_precio - (find_prod.pro_precio * (find_oferta[0].ofer_porcentaje / 100))
            dic['oferta'] = True
            dic['pre_oferta'] = format(pre_oferta, '.2f')
        # gestion productos relacionados,destacados y puntuados
        dic['productos_relacionados'] = pro_relacionados(find_prod)
        dic['productos_destacados'] = pro_mas_destacados()
        dic['productos_puntuados'] = pro_mas_puntuados()

    except Exception as e:
        dic = {}
        print e
    return render_to_response('shop/shop_product.html',dic, context_instance=RequestContext(request))

def guardar_review(request):
    if request.method == "POST":
        try:
            find_user = User.objects.get(pk = request.POST['user'])
            find_pro = producto.objects.get(pk = request.POST['pro'])
            new_review = review(
                rev_texto = request.POST['msg'],
                rev_estrellas = request.POST['val'],
                rev_usuario = find_user,
                rev_producto = find_pro)
            new_review.save()
            dic = {'result': 'OK'}
        except Exception as e:
            print e
            dic = {'result': 'ERR'}

        response = JsonResponse(dic)
        return HttpResponse(response.content)

def ver_carrito(request):
    find_transporte = emp_trasporte.objects.all()
    find_iva = impuesto.objects.get(imp_actual=True)
    dic = {
        'transportes' : find_transporte,
        'iva' : find_iva.imp_porcentaje
    }
    return render_to_response('shop/shop_cart.html',dic,context_instance=RequestContext(request))

@transaction.atomic()
def gestionar_compra(request):
    dic = {}
    if request.is_ajax():
        if request.method == 'POST':
            if request.user.is_authenticated():
                transaccion = transaction.savepoint()
                print request.POST
                try:
                    if request.POST['empresa_trans'] != 'empresa':
                        find_empresa = emp_trasporte.objects.get(trans_id=request.POST['empresa_trans'])
                    else:
                        find_empresa = None
                    new_pedido = pedido(
                        cliente = request.user,
                        ped_direccion_envio = request.POST['direccion'],
                        ped_ciudad_envio = request.POST['ciudad'],
                        ped_cod_postal_envio = request.POST['cod_postal'],
                        ped_pais_envio = 'Ecuador',
                        trans_id = find_empresa,
                        ped_total_pagar = request.POST['subtotal'],
                        ped_iva = impuesto.objects.get(imp_actual=True).imp_porcentaje
                        )
                    new_pedido.save()
                    for producto_carrito in request.POST.getlist('carrito[]'):
                        producto_carrito = json.loads(producto_carrito)
                        find_prod = producto.objects.get(pro_id=producto_carrito['ID'])
                        dic = validar_producto(find_prod, producto_carrito['Cantidad'])
                        precio_ofer = Decimal(str(producto_carrito['Precio']).replace(',','.'))
                        descuento = (find_prod.pro_precio - precio_ofer) * int(producto_carrito['Cantidad'])
                        if dic['result'] == 'OK':
                            new_det_pedido = det_pedido(
                                pro_id = find_prod,
                                ped_id = new_pedido,
                                # precio_unit = Decimal(str(producto_carrito['Precio']).replace(',','.')),
                                precio_unit = find_prod.pro_precio,
                                cantidad = producto_carrito['Cantidad'],
                                descuento = descuento,
                                total_pagar = Decimal(str(producto_carrito['Total']).replace(',','.'))
                                )
                            new_det_pedido.save()
                        else:
                            raise Exception(dic['result'], dic['mensaje'])

                    nuevo_pedido_notificacion(new_pedido)
                    transaction.savepoint_commit(transaccion)
                    dic = {'result': 'OK'}
                except Exception as e:
                    print e
                    transaction.savepoint_rollback(transaccion)
            else:
                dic = {'result' : 'LOGEATE'}
        else:
            dic = {'result': 'ERROR'}
    else:
        dic = {'result': 'ERROR'}
    response = JsonResponse(dic)
    return HttpResponse(response.content)

@login_required(login_url='/')
def gestion_pedidos(request):
    find_pedidos = pedido.objects.filter(cliente=request.user).order_by("-pk")
    dic = {
        'pedidos' : find_pedidos
    }
    return render_to_response('shop/shop_pedidos.html',dic,context_instance=RequestContext(request))

def ingresar_deposito(request):
    print request.POST
    find_pedido = pedido.objects.get(pk = request.POST['txt_id'])
    find_pedido.ped_cod_deposito = request.POST['txt_deposito']
    find_pedido.ped_estado = 'verificacion'
    try:
        find_pedido.ped_imagen_deposito = request.FILES['file1']
    except Exception as e:
        print 'no hay imagen pero igual se guarda la csm'
    find_pedido.save()
    dic = {
        'result' : 'OK'
    }
    response = JsonResponse(dic)
    return HttpResponse(response.content)

@login_required(login_url='/')
def pedido_detalle(request, id):
    try:
        find_pedido = pedido.objects.get(ped_id=id, cliente=request.user)
        find_detalle_pedido = det_pedido.objects.filter(ped_id=find_pedido)
        find_iva = impuesto.objects.get(imp_actual=True)
        if find_detalle_pedido.exists():
            subtotal = 0
            for det_ped in find_detalle_pedido:
                subtotal = subtotal + det_ped.total_pagar
            if find_pedido.trans_id != None:
                gasto_envio = find_pedido.trans_id.trans_costo
            else:
                gasto_envio = 0
            dic = {
                "pedidos" : find_detalle_pedido,
                "pedido_id" : id,
                "iva" : find_pedido.ped_iva,
                "precio_iva" : round(subtotal * (Decimal(find_pedido.ped_iva)/100), 2),
                "subtotal" : subtotal,
                "gasto_envio" : gasto_envio,
                "total_pagar" : find_pedido.ped_total_pagar
            }
        else:
            return redirect('/shop/pedidos/')
    except Exception as e:
        print e
        return redirect('/shop/pedidos/')
    return render_to_response('shop/shop_pedido_detalle.html',dic,context_instance=RequestContext(request))

def consulta_transporte(request):
    print request.POST
    try:
        find_empresa  = emp_trasporte.objects.get(pk = request.POST['id_empresa'])
        dic = {
            'result' : 'OK',
            'precio' : find_empresa.trans_costo
        }
    except Exception as e:
        print 'No hay na'
        dic = {
            'result' : 'OK',
            'precio' : Decimal(0,0)
        }
    response = JsonResponse(dic)
    return HttpResponse(response.content)