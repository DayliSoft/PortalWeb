from django.db.models import Prefetch
from django.db.models.signals import pre_save, post_save, pre_delete
from django.dispatch import receiver

from apps.shop.models import det_pedido, producto, pedido, imagen_producto
from django.core.mail import send_mail
from django.template.loader import render_to_string

def actualizar_cant_prod(sender, instance, **kwards):
    print 'actualizando cantidades de productos'

    try:
        find_prod = producto.objects.get(pro_id=instance.pro_id.pk)
        nueva_cant = int(find_prod.pro_stock) - int(instance.cantidad)
        find_prod.pro_stock = nueva_cant
        find_prod.save()
    except Exception as e:
        print e

def eliminar_pedido(sender, instance, **kwards):
    print 'vamo a restablecer cantidades'
    if instance.ped_id.ped_estado != 'enviado':
        try:
            find_prod = producto.objects.get(pk = instance.pro_id.pk)
            find_prod.pro_stock = int(find_prod.pro_stock) + int(instance.cantidad)
            find_prod.save()
        except Exception as e:
            print 'No existe el producto a restaurar las cantidad'

@receiver(pre_save, sender=pedido)
def pedido_cancelado(sender, instance, **kwargs):
    try:
        print 'restaurar productos al cancelar'

        estado_anterior = pedido.objects.get(pk=instance.ped_id).ped_estado
        estado_actual = instance.ped_estado

        if instance.ped_id and estado_anterior != estado_actual:
            instance_filter = pedido.objects.filter(ped_id=instance.pk)

            instancia_det = instance_filter.prefetch_related(
                Prefetch(
                    "det_pedido_set",
                    queryset=det_pedido.objects.filter(ped_id_id=instance.pk),
                    to_attr="detalles"
                )
            )

            for p in instancia_det[0].detalles:
                find_prod = producto.objects.get(pk=p.pro_id_id)
                if estado_actual == "cancelado":
                    find_prod.pro_stock = int(find_prod.pro_stock) + int(p.cantidad)
                    find_prod.save()

                if estado_anterior == "cancelado":
                    find_prod.pro_stock = int(find_prod.pro_stock) - int(p.cantidad)
                    find_prod.save()

    except Exception as e:
        print 'No existe el producto a restaurar las cantidad '+str(e)

post_save.connect(actualizar_cant_prod, sender=det_pedido)
pre_delete.connect(eliminar_pedido, sender=det_pedido)
post_save.connect(pedido_cancelado, sender=pedido)