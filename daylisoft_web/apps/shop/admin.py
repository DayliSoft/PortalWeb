from django.contrib import admin
from .models import *
# Register your models here.
from mptt.admin import MPTTModelAdmin
from mptt.admin import DraggableMPTTAdmin


admin.site.register(producto)
admin.site.register(pedido)
admin.site.register(review)
admin.site.register(
    categoria,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
        # ...more fields if you feel like it...
    ),
    list_display_links=(
        'indented_title',
    ),
)
admin.site.register(marca)
admin.site.register(tag)
admin.site.register(producto_cuarentena)
admin.site.register(oferta)
admin.site.register(emp_trasporte)
admin.site.register(det_pedido)
admin.site.register(proveedor)
admin.site.register(imagen_producto)