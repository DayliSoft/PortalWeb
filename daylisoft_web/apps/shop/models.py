from django.db import models
from django.conf import settings
import datetime
from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.
class marca(models.Model):
    mar_id = models.AutoField(primary_key=True)
    mar_nombre = models.CharField(max_length=50)

    def __unicode__(self):
        return self.mar_nombre

    class Meta:
        verbose_name_plural='Marcas'
        app_label = 'shop'

class categoria(MPTTModel):
    cat_id = models.AutoField(primary_key=True)
    cat_nombre = models.CharField(max_length=30)
    cat_descripcion = models.TextField()
    cat_id_padre = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)


    def __unicode__(self):
        return self.cat_nombre

    class MPTTMeta:
        parent_attr = 'cat_id_padre'
        order_insertion_by = ['cat_nombre']

    class Meta:
        verbose_name_plural='Categorias'
        app_label = 'shop'

class producto(models.Model):
    pro_id = models.AutoField(primary_key=True)
    pro_nombre = models.CharField(max_length=50)
    pro_precio_compra = models.DecimalField(max_digits=10,decimal_places=2)
    pro_precio = models.DecimalField(max_digits=10,decimal_places=2)
    pro_descripcion_corta = models.CharField(max_length=360)#cosas consisas como el peso, medidas y esas cosas
    pro_descripcion_larga = models.TextField()#toda la descripcion posible
    pro_info_adicional = models.CharField(max_length=200)
    pro_estado = models.BooleanField(default=True)
    pro_stock = models.IntegerField()
    pro_stock_minimo = models.IntegerField()
    pro_destacado = models.BooleanField(default=False)#solo pueden haber 3 destacados
    pro_valoracion = models.DecimalField(max_digits=10,decimal_places=2)
    categorias = models.ForeignKey(categoria)
    mar_id = models.ForeignKey(marca)
    tags = models.ManyToManyField('tag',related_name='pro_tags')

    def __unicode__(self):
        return self.pro_nombre

    class Meta:
        verbose_name_plural='Productos'
        app_label = 'shop'

class producto_cuarentena(models.Model):
    pro_c_id = models.AutoField(primary_key=True)
    pro_c_cantidad = models.IntegerField()
    pro_c_nota = models.TextField()
    producto = models.ForeignKey(producto)

    class Meta:
        app_label = 'shop'

class tag(models.Model):
    tag_id = models.AutoField(primary_key=True)
    tag_nombre = models.CharField(max_length=20)

    def __unicode__(self):
        return self.tag_nombre

    class Meta:
        verbose_name_plural='Tags'
        app_label = 'shop'

class review(models.Model):
    rev_id = models.AutoField(primary_key=True)
    rev_texto = models.TextField()
    rev_estrellas = models.IntegerField()
    rev_fecha = models.DateTimeField(default=datetime.datetime.now)
    rev_usuario = models.ForeignKey(settings.AUTH_USER_MODEL)
    rev_producto = models.ForeignKey(producto)

    def __unicode__(self):
        return self.rev_producto.pro_nombre

    class Meta:
        verbose_name_plural='Reviews'
        app_label = 'shop'

class oferta(models.Model):
    ofer_id = models.AutoField(primary_key=True)
    ofer_fecha_inicio = models.DateField()
    ofer_fecha_fin = models.DateField()
    ofer_porcentaje = models.DecimalField(max_digits=10,decimal_places=2)
    pro_id = models.ForeignKey(producto)

    def __unicode__(self):
        return self.pro_id.pro_nombre

    class Meta:
        verbose_name_plural='Ofertas'
        app_label = 'shop'

class emp_trasporte(models.Model):
    trans_id = models.AutoField(primary_key=True)
    trans_nombre = models.CharField(max_length=50)
    trans_costo  = models.DecimalField(max_digits=10,decimal_places=2)
    trans_url = models.CharField(max_length=50)

    def __unicode__(self):
        return self.trans_nombre

    class Meta:
        verbose_name_plural='Transportes'
        app_label = 'shop'

class pedido(models.Model):
    ped_id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(settings.AUTH_USER_MODEL)
    ped_fecha_pedido = models.DateTimeField(auto_now_add=True)
    ped_fecha_envio = models.DateTimeField(null=True,blank=True)
    ped_direccion_envio = models.TextField()
    ped_ciudad_envio = models.CharField(max_length=50)
    ped_cod_postal_envio = models.CharField(max_length=10)
    ped_pais_envio = models.CharField(max_length=20)
    ped_codigo_guia = models.CharField(max_length=50, null=True, blank=True)
    ped_estado = models.CharField(max_length=20,default='pendiente')#cancelado, pendiente, verificacion, enviado
    ped_cod_deposito = models.CharField(max_length=30, null=True, blank=True)
    ped_imagen_deposito = models.ImageField(upload_to = 'depositos', null=True,blank=True,default='/static/daylisoft/img/sin_imagen.gif')
    trans_id = models.ForeignKey(emp_trasporte, null=True, blank=True)
    ped_total_pagar = models.DecimalField(max_digits=10,decimal_places=2)
    ped_iva = models.DecimalField(max_digits=10,decimal_places=2,null=True,blank=True)

    def __unicode__(self):
        return self.cliente.us_nombre+'-'+str(self.ped_fecha_pedido)

    class Meta:
        verbose_name_plural='Pedidos'
        app_label = 'shop'

class det_pedido(models.Model):
    pro_id = models.ForeignKey(producto)
    ped_id = models.ForeignKey(pedido)
    precio_unit = models.DecimalField(max_digits=10,decimal_places=2)
    cantidad = models.IntegerField()
    descuento = models.DecimalField(max_digits=10,decimal_places=2)
    total_pagar = models.DecimalField(max_digits=10,decimal_places=2)

    class Meta:
        verbose_name_plural='Detalle pedidos'
        app_label = 'shop'

class proveedor(models.Model):
    prov_id = models.AutoField(primary_key=True)
    prov_nombre = models.CharField(max_length=50)
    prov_contacto = models.CharField(max_length=50)
    prov_direccion = models.CharField(max_length=50)
    prov_ciudad = models.CharField(max_length=20)
    prov_pais = models.CharField(max_length=10)
    prov_telefono = models.CharField(max_length=10)
    prov_cel = models.CharField(max_length=10)
    prov_home_page = models.CharField(max_length=30,blank=True,null=True)
    prov_estado = models.BooleanField(default=True)

    def __unicode__(self):
        return self.prov_nombre

    class Meta:
        verbose_name_plural='Proveedores'
        app_label = 'shop'

class imagen_producto(models.Model):
    img_id = models.AutoField(primary_key=True)
    img_data = models.ImageField(upload_to = 'imagenes_productos')
    img_es_principal = models.BooleanField(default=False)
    pro_id = models.ForeignKey(producto)

    def __unicode__(self):
        return self.pro_id.pro_nombre

    class Meta:
        verbose_name_plural='Imagenes productos'
        app_label = 'shop'
