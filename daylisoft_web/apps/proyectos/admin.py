from django.contrib import admin
from .models import *
from django.forms import ModelForm

from django.forms import ModelForm, TextInput
from suit.widgets import LinkedSelect
from suit.widgets import SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget

from django.contrib.admin import ModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
# Register your models here.
admin.site.register(categoriaProyecto)
admin.site.register(proyecto)
admin.site.register(ImagenesProyecto)

class PageFormTextos(ModelForm):
    class Meta:
        widgets = {
            'etexp_texto': CKEditorWidget(editor_options={'startupFocus': True})
        }

class PageAdminTextos(ModelAdmin):
    form = PageFormTextos
    fieldsets = [
    ('Textos para el blog', {
        'fields': ['etexp_proyecto'],
    }),
      ('Ingrese el texto', {'classes': ('full-width',), 'fields': ('etexp_texto',)}),

    ]

admin.site.register(entradasTextosProyecto, PageAdminTextos)