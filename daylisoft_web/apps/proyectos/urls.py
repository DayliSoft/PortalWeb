from django.conf.urls import patterns, url
from .views import *
urlpatterns = patterns('',
    url(r'^categoria/(?P<cat>\w+)/(?P<pro>\d+)/$', proyecto_detalles, name='proyectoDetalle'),
    url(r'^inicio/', cargar_proyectos, name='cargarProyectos'),
)