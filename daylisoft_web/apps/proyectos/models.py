from django.db import models
from django.conf import settings
from apps.users.models import User
from apps.shop.models import proveedor
from PIL import Image
# from django.contrib.auth.models import User, Group

class categoriaProyecto(models.Model):
    catp_id = models.AutoField(primary_key=True)
    catp_nombre = models.CharField('Nombre', max_length=40)

    def __unicode__(self):
        return self.catp_nombre

class proyecto(models.Model):
    pro_id = models.AutoField(primary_key=True)
    pro_proveedor = models.ForeignKey(proveedor)
    pro_titulo = models.CharField('Titulo', max_length=40, blank=True)
    pro_fecha = models.DateTimeField('Fecha',auto_now_add=True)
    pro_estado = models.BooleanField('Estado', default=True)
    pro_imagen= models.FileField('Archivo', upload_to = 'proyectos', null=True,blank=True)
    pro_categoria = models.ForeignKey(categoriaProyecto)
    def __unicode__(self):
        return self.pro_titulo

    def save(self):
        if not self.pro_imagen:
            return

        super(proyecto, self).save()
        pro_imagen = Image.open(self.pro_imagen)
        ancho, alto = pro_imagen.size
        ratio_height = (50*alto)/ancho
        size = (370, 300)
        pro_imagen = pro_imagen.resize(size, Image.ANTIALIAS)
        pro_imagen.save(self.pro_imagen.path)

class entradasTextosProyecto(models.Model):
    etexp_id = models.AutoField(primary_key=True)
    etexp_proyecto = models.ForeignKey(proyecto, related_name='textosProyectos')
    etexp_fecha = models.DateTimeField(auto_now_add=True)
    etexp_texto = models.TextField('Texto', max_length=500)

    def __unicode__(self):
        return self.etexp_proyecto.pro_titulo

class ImagenesProyecto(models.Model):
    imap_id=models.AutoField(primary_key=True)
    imgp_proyecto = models.ForeignKey(proyecto, related_name='imagesp')
    imgp_imagen = models.ImageField('Imagen Proyectos', upload_to = 'proyectos')

    def __unicode__(self):
        return self.imgp_proyecto.pro_titulo