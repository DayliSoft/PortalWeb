#-*- coding: utf-8 -*-
import json
from django.views.decorators.csrf import *
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect
from .models import *
from apps.portal.models import redesSociales
from apps.blog.models import entrada
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def cargar_proyectos(request):
    context={}
    page = request.GET.get('page')
    context['categorias']= categoriaProyecto.objects.all()
    context['proyectos']= proyecto.objects.all()
    context['redes']=redesSociales.objects.all()
    return render_to_response('proyectos/list_proyectos.html', context, context_instance=RequestContext(request))

def proyecto_detalles(request, cat, pro):
    try:
        context={}
        proy=proyecto.objects.get(pro_id=pro)
        textos=entradasTextosProyecto.objects.filter(etexp_proyecto=proy.pro_id).order_by('-etexp_fecha')
        context['ultimosProyectos']=proyecto.objects.all().order_by('-pro_fecha')[0:4]
        context['seleccionProyecto']=proy
        context['redes']=redesSociales.objects.all()
        page = request.GET.get('page')
        paginator = Paginator(textos, 2)
        context['proyectoTextos'] = paginator.page(page)
        return render_to_response('proyectos/detalle_proyecto.html', context, context_instance=RequestContext(request))
    except PageNotAnInteger:
        context['proyectoTextos'] = paginator.page(1)
        return render_to_response('proyectos/detalle_proyecto.html', context, context_instance=RequestContext(request))
    except EmptyPage:
        context['proyectoTextos'] = paginator.page(paginator.num_pages)
        return render_to_response('proyectos/detalle_proyecto.html', context, context_instance=RequestContext(request))
    except Exception as e:
        print(e)
        print('No existe ese proyecto en esa categoria')
        return cargar_proyectos(request)

