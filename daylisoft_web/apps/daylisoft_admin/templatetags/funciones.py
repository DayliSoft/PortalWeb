#-*- coding: utf-8 -*-
from django.template import Library
register = Library()

@register.filter(name='dividir')
def dividir(value, iva):
    valor = float(value)/float(1+(iva/100))
    return round(valor, 2)

@register.filter(name='multiplicar')
def multiplicar(total, iva):
    valor = dividir(total, iva)*float(iva/100)
    return round(valor, 2)