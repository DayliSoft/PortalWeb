from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(perfil_empresa)
admin.site.register(impuesto)
admin.site.register(campaign_email)
admin.site.register(historial_campaign_email)