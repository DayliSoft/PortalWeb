from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
    url(r'^$', inicio, name="url_inicio"),

    url(r'^productos/$', listar_productos, name="url_listar_productos"),
    url(r'^productos/agregar/$', form_agregar_producto, name="url_form_agregar_producto"),
    url(r'^productos/agregar/(?P<id_prod>\d+)/$', form_editar_producto, name="url_form_editar_producto"),
    url(r'^productos/add_1', agregar_productos_p1, name="url_agregar_productos_p1"),
    url(r'^productos/add_2', agregar_productos_p2, name="url_agregar_productos_p2"),
    url(r'^productos/add_3', agregar_productos_p3, name="url_agregar_productos_p3"),
    url(r'^productos/add_4', agregar_productos_p4, name="url_agregar_productos_p4"),
    url(r'^productos/eliminar', eliminar_productos, name="url_eliminar_productos"),
    url(r'^productos/filtrar', filtrar_nombre_producto, name="url_filtrar_nombre_producto"),

    url(r'^productos/cuarentena/$', productos_cuarentena, name="url_productos_cuarentena"),
    url(r'^productos/cuarentena/cargar', cargar_cuarentena, name="url_cargar_cuarentena"),
    url(r'^productos/cuarentena/agregar', agregar_cuarentena, name="url_agregar_cuarentena"),
    url(r'^productos/cuarentena/restaurar', restaurar_cuarentena, name="url_restaurar_cuarentena"),

    url(r'^productos/categoria/$', listar_categorias_tabla, name="url_listar_categorias_tabla"),
    url(r'^productos/categorias/select', cargar_categorias_select, name="url_cargar_categorias_select"),
    url(r'^productos/categorias/agregar', agregar_categoria, name="url_agregar_categoria"),
    url(r'^productos/categorias/cargar', cargar_categoria, name="url_cargar_categoria"),
    url(r'^productos/categorias/eliminar', eliminar_categoria, name="url_eliminar_categoria"),

    url(r'^productos/marcas/$', listar_marcas, name="url_listar_marcas"),
    url(r'^productos/marcas/agregar', agregar_marca, name="url_agregar_marca"),
    url(r'^productos/marcas/cargar', cargar_marca, name="url_cargar_marca"),
    url(r'^productos/marcas/eliminar', eliminar_marca, name="url_eliminar_marca"),

    url(r'^productos/tags/$', listar_tags, name="url_listar_tags"),
    url(r'^productos/tags/agregar', agregar_tags, name="url_agregar_tags"),
    url(r'^productos/tags/cargar', cargar_tags, name="url_cargar_tags"),
    url(r'^productos/tags/eliminar', eliminar_tags, name="url_eliminar_tags"),

    url(r'^productos/imagenes/$', listar_imagenes, name="url_listar_imagenes"),
    url(r'^productos/imagenes/eliminar', eliminar_imagenes, name="url_eliminar_imagenes"),
    url(r'^productos/imagenes/cambiar', cambiar_imagenes, name="url_cambiar_imagenes"),
    url(r'^productos/imagenes/subir', subir_nueva_imagen, name="url_subir_nueva_imagen"),

    url(r'^productos/ofertas/$', listar_ofertas, name="url_listar_ofertas"),
    url(r'^productos/ofertas/agregar', agregar_ofertas, name="url_agregar_ofertas"),
    url(r'^productos/ofertas/cargar', cargar_ofertas, name="url_cargar_ofertas"),
    url(r'^productos/ofertas/eliminar', eliminar_ofertas, name="url_eliminar_ofertas"),

    url(r'^productos/reviews/$', listar_reviews, name="url_listar_reviews"),
    url(r'^productos/reviews/eliminar', eliminar_reviews, name="url_eliminar_reviews"),

    url(r'^proveedores/$', listar_proveedores, name="url_listar_proveedores"),
    url(r'^proveedores/agregar/$', form_agregar_proveedor, name="url_form_agregar_proveedor"),
    url(r'^proveedores/agregar/(?P<id_prov>\d+)/$', form_editar_proveedor, name="url_form_editar_proveedor"),
    url(r'^proveedores/eliminar', eliminar_proveedores, name="url_eliminar_proveedores"),
    url(r'^proveedores/add', agregar_proveedor, name="url_agregar_proveedor"),
    url(r'^proveedores/activar', activar_proveedor, name="url_activar_proveedor"),

    url(r'^clientes/$', listar_clientes, name="url_listar_clientes"),
    url(r'^clientes/agregar/$', form_agregar_cliente, name="url_form_agregar_cliente"),
    url(r'^clientes/agregar/(?P<id_cli>\d+)/$', form_editar_cliente, name="url_form_editar_cliente"),
    url(r'^clientes/add', agregar_cliente, name="url_agregar_cliente"),
    url(r'^clientes/eliminar', eliminar_clientes, name="url_eliminar_clientes"),
    url(r'^clientes/activar', activar_clientes, name="url_activar_clientes"),
    url(r'^clientes/superusuario', superusuario_cliente, name="url_superusuario_cliente"),
    url(r'^clientes/cambio', cambio_clave_cliente, name="url_cambio_clave_cliente"),

    url(r'^transporte/$', listar_transporte, name="url_listar_transporte"),
    url(r'^transporte/agregar', agregar_transporte, name="url_agregar_transporte"),
    url(r'^transporte/cargar', cargar_transporte, name="url_cargar_transporte"),
    url(r'^transporte/eliminar', eliminar_transporte, name="url_eliminar_transporte"),

    url(r'^impuesto/$', listar_impuestos, name="url_listar_impuestos"),
    url(r'^impuesto/agregar', agregar_impuestos, name="url_agregar_impuestos"),
    url(r'^impuesto/cargar', cargar_impuestos, name="url_cargar_impuestos"),
    url(r'^impuesto/eliminar', eliminar_impuestos, name="url_eliminar_impuestos"),
    url(r'^impuesto/actual', impuesto_actual, name="url_impuesto_actual"),

    url(r'^empresa/$', form_perfil_empresa, name="url_perfil_empresa"),
    url(r'^empresa/actualizar', actualizar_perfil_empresa, name="url_actualizar_perfil_empresa"),

    url(r'^pedidos/$', listar_pedidos, name="url_listar_pedidos"),
    url(r'^pedidos/(?P<id_ped>\d+)/$', ver_detalle_pedido, name="url_ver_detalle_pedido"),
    url(r'^pedidos/deposito', cargar_deposito, name="url_cargar_deposito"),
    url(r'^pedidos/cliente', cargar_ped_cliente, name="url_cargar_ped_cliente"),
    url(r'^pedidos/estado', cambiar_estado_pedidos, name="url_cambiar_estado_pedidos"),
    url(r'^pedidos/eliminar', eliminar_pedido, name="url_eliminar_pedido"),

    url(r'^notificaciones/$', listar_productos_bajo_stock, name="url_listar_productos_bajo_stock"),
    url(r'^notificaciones/pedidos/$', listar_pedidos_pendientes, name="url_listar_pedidos_pendientes"),

    url(r'^email/$', listar_email, name="url_listar_email"),
    url(r'^email/agregar/$', form_agregar_email, name="url_form_agregar_email"),
    url(r'^email/agregar/(?P<id_email>\d+)/$', form_editar_email, name="url_form_editar_email"),
    url(r'^email/historial/$', listar_historial_email, name="url_listar_historial_email"),

    url(r'^tags/', cargar_todos_tags, name="url_cargar_todos_tags"),

    url(r'^consulta/productos/', json_productos, name="url_json_productos"),

    url(r'^compras/$', reporte_compras, name="url_reporte_compras"),
    url(r'^ventas/$', reporte_ventas, name="url_reporte_ventas"),
)