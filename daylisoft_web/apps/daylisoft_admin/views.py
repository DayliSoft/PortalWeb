#-*- coding: utf-8 -*-
from django.utils.translation import gettext
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseRedirect, HttpResponse
import json
from apps.shop.models import *
from django.db.models import Sum, F, Prefetch, Q
from .models import *
from apps.users.models import User
from django.db import transaction
import os
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate

# Pagina de inicio
def inicio(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    meses = [gettext('Enero'), gettext('Febrero'), gettext('Marzo'), gettext('Abril'), gettext('Mayo'), gettext('Junio'),
             gettext('Julio'), gettext('Agosto'), gettext('Septiembre'), gettext('Octubre'), gettext('Noviembre'), gettext('Diciembre')]
    valores = [8250, 7000, 6300, 8800, 6600, 9500, 2999, 7980, 8100, 6402, 9631, 10100]
    label_ventas = gettext("Ventas: 1 de Enero 2016 - 31 de Diciembre 2016").decode("utf-8")
    parametros = [20000, 14000, 6000]

    productos = producto.objects.all().order_by("-pro_id").prefetch_related(
        Prefetch(
            "imagen_producto_set",
            queryset= imagen_producto.objects.filter(img_es_principal=True),
            to_attr="pro_imagen_principal"
        )
    )

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()

    alerta = None
    if productos_bajo_stock or pedidos_pendientes:
        alerta = gettext("Existen notificaciones que necesitan ser atendidas").decode("utf-8")

    dic = {
        'productos': productos,
        'pedidos': pedido.objects.all().order_by("-ped_fecha_pedido"),
        'facturas': marca.objects.all(),
        'clientes': User.objects.all().exclude(is_superuser=True),
        'label_ventas': label_ventas,
        'meses': meses,
        'valores': valores,
        'parametros': parametros,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
        'alerta_notificaciones': alerta
    }

    return render(request, "dashboard/inicio.html", dic)

# Productos ************************************************************************************************************
def form_agregar_producto(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_destacados = producto.objects.filter(pro_destacado=True).count()
    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'categorias': categoria.objects.all(),
        'marcas': marca.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
        'total_destacados': productos_destacados
    }
    return render(request, "dashboard/productos/agregar.html", dic)

def form_editar_producto(request, id_prod):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    producto_editar = None
    pro_tag = None

    if id_prod:
        if producto.objects.filter(pro_id=id_prod).exists():
            producto_editar = producto.objects.filter(pro_id=id_prod).prefetch_related(
                Prefetch(
                    "imagen_producto_set",
                    queryset=imagen_producto.objects.all(),
                    to_attr="pro_imagen_principal"
                ),
                Prefetch(
                    "oferta_set",
                    queryset=oferta.objects.all().order_by("-ofer_fecha_fin"),
                    to_attr="pro_oferta"
                )
            )
            pro_tag = tag.objects.filter(pro_tags__pro_id=id_prod)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_productos"))

    productos_destacados = producto.objects.filter(pro_destacado=True).count()
    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'producto_editar': producto_editar,
        'num_imagenes': (3 - producto_editar[0].imagen_producto_set.count()),
        'pro_tag': pro_tag,
        'categorias': categoria.objects.all(),
        'marcas': marca.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
        'total_destacados': productos_destacados
    }
    return render(request, "dashboard/productos/agregar.html", dic)

def listar_productos(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    id_producto = request.GET.get('id')
    producto_detalle = None
    tags_detalle = None

    if producto.objects.filter(pro_id = id_producto).exists():
        producto_detalle = producto.objects.filter(pro_id = id_producto).prefetch_related(
            Prefetch(
                "imagen_producto_set",
                queryset= imagen_producto.objects.filter(img_es_principal=True),
                to_attr="pro_imagen_principal"
            ),
            Prefetch(
                "oferta_set",
                queryset=oferta.objects.all().order_by("-ofer_fecha_fin"),
                to_attr="pro_oferta"
            )
        )
        tags_detalle= tag.objects.filter(pro_tags__pro_id = id_producto)

    productos = producto.objects.all().prefetch_related(
        Prefetch(
            "imagen_producto_set",
            queryset= imagen_producto.objects.filter(img_es_principal=True),
            to_attr="pro_imagen_principal"
        )
    )

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()

    produ = productos
    if not request.user.is_superuser:
        produ = productos.exclude(pro_estado=False)

    dic = {
        'productos': produ,
        'notif_stock': productos_bajo_stock,
        'producto_detalles': producto_detalle,
        'tags_detalle': tags_detalle,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/administrar.html", dic)

def agregar_productos_p1(request):
    datos = {}
    nombre = request.POST.get('nombre_producto')
    categoria = request.POST.get('categoria_producto')
    marca = request.POST.get('id_marca_producto')
    estado = request.POST.get('estado_producto', False)
    d_corta = request.POST.get('descripcion_producto_corta')
    d_larga = request.POST.get('descripcion_producto_larga', gettext("Ninguna").decode("utf-8"))
    informacion = request.POST.get('informacion_ad_producto', gettext("Ninguna").decode("utf-8"))

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')

    try:
        listaRegistrados = producto.objects.all()
        if id_prod:
            listaRegistrados = listaRegistrados.exclude(pro_id=id_prod)

        for l in listaRegistrados:
            if (l.pro_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_prod:
            prod = producto(pro_nombre=nombre, categorias_id=categoria, mar_id_id=marca, pro_estado=estado,
                            pro_descripcion_corta=d_corta, pro_descripcion_larga=d_larga, pro_info_adicional=informacion,
                            pro_precio_compra=0, pro_precio=0, pro_stock=0, pro_stock_minimo=0, pro_valoracion=0)
            prod.save()
        else:
            if not producto.objects.filter(pro_id=id_prod).exists():
                raise Exception("Error", "No existe el producto")

            prod = producto.objects.get(pro_id=id_prod)
            prod.pro_nombre = nombre
            prod.categorias_id = categoria
            prod.mar_id_id = marca
            prod.pro_estado = estado
            prod.pro_descripcion_corta = d_corta
            prod.pro_descripcion_larga = d_larga
            prod.pro_info_adicional = informacion

            prod.save()
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datos['message'] = mensaje
        datos['id'] = prod.pro_id
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def agregar_productos_p2(request):
    datos = {}
    compra = request.POST.get('precio_compra_producto')
    venta = request.POST.get('precio_venta_producto')
    stock = request.POST.get('stock_producto')
    stock_min = request.POST.get('stock_min_producto')

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')

    try:
        if not producto.objects.filter(pro_id=id_prod).exists():
            raise Exception("Error", "No existe el producto")

        prod = producto.objects.get(pro_id=id_prod)
        prod.pro_precio_compra = compra
        prod.pro_precio = venta
        prod.pro_stock = stock
        prod.pro_stock_minimo = stock_min

        prod.save()

        datos['message'] = mensaje
        datos['id'] = prod.pro_id
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def agregar_productos_p3(request):
    datos = {}
    fecha_inicio = request.POST.get('producto_fecha_inicio')
    fecha_fin = request.POST.get('producto_fecha_fin')
    porcentaje = request.POST.get('producto_precio_oferta')
    id_oferta = request.POST.get('producto_id_oferta')

    #Formato de fecha correcto
    fecha_inicio =  datetime.datetime.strptime(fecha_inicio, "%Y/%m/%d").strftime("%Y-%m-%d")
    fecha_fin = datetime.datetime.strptime(fecha_fin, "%Y/%m/%d").strftime("%Y-%m-%d")

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')

    try:
        if not producto.objects.filter(pro_id=id_prod).exists():
            raise Exception("Error", "No existe el producto")

        if not id_oferta:
            ofer = oferta(ofer_fecha_inicio=fecha_inicio, ofer_fecha_fin=fecha_fin, ofer_porcentaje=porcentaje, pro_id_id=id_prod)
            ofer.save()
        else:
            ofer = oferta.objects.get(ofer_id=id_oferta)
            ofer.ofer_fecha_inicio = fecha_inicio
            ofer.ofer_fecha_fin = fecha_fin
            ofer.ofer_porcentaje = porcentaje

            ofer.save()
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datos['message'] = mensaje
        datos['id'] = id_prod
        datos['id_oferta'] = ofer.ofer_id
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def agregar_productos_p4(request):
    datos = {}
    imagenes = request.FILES.getlist('id_imagenes_producto[]')
    destacado = request.POST.get('producto_destacado', False)
    valoracion = request.POST.get('producto_valoracion', 0)

    str_etiquetas = request.POST.get('tags_producto')
    etiquetas = str(str_etiquetas).split(",")

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')
    id_imagen = request.POST.get('producto_id_imagen')
    id_radio_imagen_principal = request.POST.get('imagen_principal')

    # Para ocultar el boton de restaurar imagenes (cuando actualice las imagenes) 0: no se actualizo | 1: se actualizaron
    bandera_imagen = 0

    transaccion = transaction.savepoint()
    try:
        if not producto.objects.filter(pro_id=id_prod).exists():
            raise Exception("Error", "No existe el producto")

        prod = producto.objects.get(pro_id=id_prod)

        # Valida que solo acepte 3 productos destacados
        if producto.objects.filter(pro_destacado=True).count() >= 3 and destacado == "on" and prod.pro_destacado == False:
            datos['message'] = gettext("No puede agregar más de 3 productos destacados.").decode("utf-8")
            datos['result'] = "X"
            return HttpResponse(json.dumps(datos), content_type="application/json")

        # Actualizar campos en producto
        prod.pro_destacado = destacado
        prod.pro_valoracion = valoracion

        prod.save()

        # Guardar las imagenes y poner la primera como principal
        if imagenes:
            contador = 0

            if id_imagen:
                # Eliminar las imagenes anteriores de la carpeta media
                eliminadas = imagen_producto.objects.filter(pro_id=id_prod)

                if (len(imagenes)+eliminadas.count()) > 3:
                    for el in eliminadas:
                        if borrar_archivos_media(el.img_data.path):
                            el.delete()
                else:
                    contador = 1

                bandera_imagen = 1

            for i in imagenes:
                principal = False

                if contador == 0:
                    principal = True

                ima = imagen_producto(img_data=i, img_es_principal=principal, pro_id_id=id_prod)
                ima.save()
                contador = contador+1
        else:
            if id_imagen:
                todas = imagen_producto.objects.filter(pro_id_id=id_prod)
                if todas.count() > 0:
                    for ima in todas:
                        principal = False

                        if ima.img_id == int(id_radio_imagen_principal):
                            principal = True

                        ima.img_es_principal = principal
                        ima.save()

        # Guardar tags
        todos_tags = prod.tags
        if todos_tags.count() > 0:
            todos_tags.clear()

        todos_tags.add(*etiquetas)

        datos['message'] = mensaje
        datos['id'] = prod.pro_id
        datos['bandera_imagen'] = bandera_imagen
        datos['id_imagen'] = id_imagen
        datos['result'] = "OK"

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"
        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_productos(request):
    datos = {}
    id_prod = request.GET.get("id")

    try:
        obj_prod = producto.objects.get(pro_id=id_prod)
        obj_prod.pro_estado = False
        obj_prod.save()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Productos en cuarentena **********************************************************************************************
def productos_cuarentena(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'productos_cuarentena': producto_cuarentena.objects.all(),
        'productos': producto.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/cuarentena.html", dic)

@transaction.atomic()
def agregar_cuarentena(request):
    datos = {}
    cantidad = request.POST.get('cuarentena_cantidad')
    nota = request.POST.get('cuarentena_nota')
    id_producto = request.POST.get('id_producto_cuarentena')
    id_cuarentena = request.POST.get('cuarentena_id')
    producto_editar_id = request.POST.get('producto_editar_id')

    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    cantidad_total = int(cantidad)

    transaccion = transaction.savepoint()
    stock_cuarentena = 0
    try:
        if not id_cuarentena:
            add_producto = producto.objects.get(pro_id=int(id_producto))
            stock_producto_actual = add_producto.pro_stock
        else:
            add_producto = producto.objects.get(pro_id=int(producto_editar_id))
            stock_cuarentena = producto_cuarentena.objects.get(pro_c_id=int(id_cuarentena)).pro_c_cantidad
            stock_producto_actual = add_producto.pro_stock + stock_cuarentena

        if int(cantidad) > stock_producto_actual:
            datos['message'] = gettext("No existe sufiencie stock del producto").decode("utf-8")+". Actualmente: "+str(add_producto.pro_stock)
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_cuarentena:
            cuarentena = producto_cuarentena(pro_c_cantidad=int(cantidad), pro_c_nota=nota, producto=add_producto)
        else:
            cantidad_total = int(cantidad) - stock_cuarentena
            cuarentena = producto_cuarentena(pro_c_id=int(id_cuarentena), pro_c_cantidad=int(cantidad), pro_c_nota=nota, producto=add_producto)

            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        cuarentena.save()

        #resta del stock del producto
        add_producto.pro_stock = add_producto.pro_stock - cantidad_total
        add_producto.save()

        categoria_cuar = add_producto.categorias.cat_nombre

        datos['message'] = mensaje
        datos['id'] = cuarentena.pro_c_id
        datos['cantidad'] = cantidad
        datos['nota'] = nota
        datos['nombre_producto'] = add_producto.pro_nombre
        datos['categoria'] = categoria_cuar
        datos['result'] = "OK"

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"
        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_cuarentena(request):
    datos = {}
    id_cuarentena = request.GET.get("id")

    try:
        cuarentena = producto_cuarentena.objects.get(pro_c_id=int(id_cuarentena))
        datos['id'] = id_cuarentena
        datos['cantidad'] = cuarentena.pro_c_cantidad
        datos['nota'] = cuarentena.pro_c_nota
        datos['producto_id'] = cuarentena.producto.pro_id
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def restaurar_cuarentena(request):
    datos = {}
    id_cuarentena = request.GET.get("id")

    transaccion = transaction.savepoint()
    try:
        prod_eliminar = producto_cuarentena.objects.get(pro_c_id=id_cuarentena)
        id_producto = prod_eliminar.producto.pro_id
        stock_producto = prod_eliminar.pro_c_cantidad

        prod_eliminar.delete()

        #restaura el stock del producto
        prod_restaurar = producto.objects.get(pro_id=id_producto)
        prod_restaurar.pro_stock = prod_restaurar.pro_stock + stock_producto
        prod_restaurar.save()

        datos['result'] = "OK"
        datos['message'] = gettext("El producto ha sido restaurado correctamente").decode("utf-8")

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Categorias y subcategorias *******************************************************************************************
def listar_categorias_tabla(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'categorias': categoria.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/categorias.html", dic)

def agregar_categoria(request):
    datos = {}
    nombre = request.POST.get('id_categoria_hijo')
    descripcion = request.POST.get('categoria_descripcion')
    padre = request.POST.get('id_categoria_padre')
    id_categ = request.POST.get('categoria_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    try:
        listaRegistrados = categoria.objects.all()
        if id_categ:
            listaRegistrados = listaRegistrados.exclude(cat_id=id_categ)

        for l in listaRegistrados:
            if (l.cat_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_categ:
            if not padre:
                categ = categoria(cat_nombre=nombre, cat_descripcion=descripcion)
            else:
                categ = categoria(cat_nombre=nombre, cat_descripcion=descripcion, cat_id_padre = categoria.objects.get(cat_id=padre))
        else:
            categ = categoria.objects.get(cat_id=id_categ)
            categ.cat_nombre = nombre
            categ.cat_descripcion = descripcion

            if padre:
                categ.cat_id_padre = categoria.objects.get(cat_id=padre)

            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        categ.save()

        datos['message'] = mensaje
        datos['id'] = categ.cat_id
        datos['nombre'] = categ.cat_nombre

        if padre:
            datos['padre'] = categ.cat_id_padre.cat_id
            datos['padre_nombre'] = categ.cat_id_padre.cat_nombre

        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_categoria(request):
    datos = {}
    id_categ = request.GET.get("id")

    try:
        categ = categoria.objects.get(cat_id=id_categ)

        datos['id'] = categ.cat_id
        datos['nombre'] = categ.cat_nombre
        datos['descripcion'] = categ.cat_descripcion
        datos['padre'] = categ.cat_id_padre_id
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_categoria(request):
    datos = {}
    id_categ = request.GET.get("id")

    try:
        obj_cat = categoria.objects.get(cat_id=id_categ)
        obj_cat.delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_categorias_select(request):
    datos = []
    categorias = categoria.objects.all()
    for a in categorias:
        detalle = obtenerPadre(a)
        datos.append({'id': a.cat_id, 'text': a.cat_nombre+detalle})

    return HttpResponse(json.dumps(datos), content_type="application/json")

def obtenerPadre(a):
    detalle = ""
    if a.cat_id_padre_id == 0:
        detalle = ""
    else:
        bandera = False
        while(bandera == False):
            if (a.cat_id_padre_id > 0):
                categ = categoria.objects.get(cat_id=a.cat_id_padre_id)
                detalle = (detalle)+", "+categ.cat_nombre
                a = categ
            else:
                detalle = (detalle)+""
                bandera = True

    return detalle

# Marcas ***************************************************************************************************************
def listar_marcas(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'marcas': marca.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/marcas.html", dic)

def agregar_marca(request):
    datos = {}
    nombre = request.POST.get('marca_producto')
    id_mar = request.POST.get('marca_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    try:
        listaRegistrados = marca.objects.all()
        if id_mar:
            listaRegistrados = listaRegistrados.exclude(mar_id=id_mar)

        for l in listaRegistrados:
            if (l.mar_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_mar:
            ma = marca(mar_nombre=nombre)
        else:
            ma = marca.objects.get(mar_id=id_mar)
            ma.mar_nombre = nombre

            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        ma.save()

        datos['message'] = mensaje
        datos['id'] = ma.mar_id
        datos['nombre'] = ma.mar_nombre
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_marca(request):
    datos = {}
    id_mar = request.GET.get("id")

    try:
        ma = marca.objects.get(mar_id=id_mar)

        datos['id'] = ma.mar_id
        datos['nombre'] = ma.mar_nombre
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_marca(request):
    datos = {}
    id_mar = request.GET.get("id")

    try:
        obj_mar = marca.objects.get(mar_id=id_mar)
        obj_mar.delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

#Proveedores ***********************************************************************************************************
def form_agregar_proveedor(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/proveedor/agregar.html", dic)

def form_editar_proveedor(request, id_prov):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    proveedor_editar = None
    if id_prov:
        if proveedor.objects.filter(prov_id=id_prov).exists():
            proveedor_editar = proveedor.objects.get(prov_id=id_prov)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_proveedores"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'proveedor_editar': proveedor_editar,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/proveedor/agregar.html", dic)

def listar_proveedores(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    prove = proveedor.objects.all()
    if not request.user.is_superuser:
        prove = proveedor.objects.all().exclude(prov_estado=False)

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'proveedores': prove,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/proveedor/administrar.html", dic)

def agregar_proveedor(request):
    datos = {}
    nombre = request.POST.get('proveedor_nombre')
    contacto = request.POST.get('proveedor_contacto')
    direccion = request.POST.get('proveedor_direccion')
    ciudad = request.POST.get('proveedor_ciudad')
    pais = request.POST.get('proveedor_pais')
    telefono = request.POST.get('proveedor_telefono')
    celular = request.POST.get('proveedor_celular')
    pagina = request.POST.get('proveedor_pagina')
    id_proveedor = request.POST.get('id_proveedor')

    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    try:
        listaRegistrados = proveedor.objects.all()
        if id_proveedor:
            listaRegistrados = listaRegistrados.exclude(prov_id=id_proveedor)

        for l in listaRegistrados:
            if (l.prov_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_proveedor:
            prov = proveedor(prov_nombre=nombre, prov_contacto=contacto, prov_direccion=direccion, prov_ciudad=ciudad, prov_pais=pais, prov_telefono=telefono, prov_cel=celular, prov_home_page=pagina)
        else:
            prov = proveedor.objects.get(prov_id=id_proveedor)
            prov.prov_nombre = nombre
            prov.prov_contacto = contacto
            prov.prov_direccion = direccion
            prov.prov_ciudad = ciudad
            prov.prov_pais = pais
            prov.prov_telefono = telefono
            prov.prov_cel = celular
            prov.prov_home_page = pagina

            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        prov.save()

        datos['message'] = mensaje
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_proveedores(request):
    datos = {}
    id_prv = request.GET.get("id")

    try:
        obj_prv = proveedor.objects.get(prov_id=id_prv)
        obj_prv.prov_estado = False
        obj_prv.save()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def activar_proveedor(request):
    datos = {}
    id_prov = request.GET.get("id")

    try:
        obj_prv = proveedor.objects.get(prov_id=id_prov)
        obj_prv.prov_estado = True

        obj_prv.save()

        datos['result'] = "OK"
        datos['id'] = obj_prv.prov_id
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Clientes *************************************************************************************************************
def form_agregar_cliente(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/cliente/agregar.html", dic)

def form_editar_cliente(request, id_cli):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    cliente_editar = None
    if id_cli:
        if User.objects.filter(id=id_cli).exists():
            cliente_editar = User.objects.get(id=id_cli)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_clientes"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'cliente_editar': cliente_editar,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/cliente/agregar.html", dic)

def agregar_cliente(request):
    datos = {}
    usuario = request.POST.get('cliente_usuario')
    contrasena = request.POST.get('cliente_contrasena')
    nombre = request.POST.get('cliente_nombre')
    apellido = request.POST.get('cliente_apellido')
    email = request.POST.get('cliente_email')
    telefono = request.POST.get('cliente_telefono')
    avatar = request.FILES.get('id_avatar_usuario')
    id_cliente = request.POST.get('id_cliente')

    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    bandera = "A"

    try:
        listaRegistrados = User.objects.all()
        if id_cliente:
            listaRegistrados = listaRegistrados.exclude(id=id_cliente)

        for l in listaRegistrados:
            if (l.username == usuario or l.email == email):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre de usuario o email").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_cliente:
            cliente = User(username=usuario, password=make_password(contrasena), us_nombre=nombre, us_apellidos=apellido, us_telefono_movil=telefono, email=email, us_avatar=avatar)
        else:
            cliente = User.objects.get(id=id_cliente)
            cliente.username = usuario
            cliente.us_nombre = nombre
            cliente.us_apellidos = apellido
            cliente.us_telefono_movil = telefono
            cliente.email = email

            if avatar:
                if cliente.us_avatar:
                    os.remove(cliente.us_avatar.path)

                cliente.us_avatar = avatar

            bandera = "E"
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        cliente.save()

        datos['message'] = mensaje
        datos['bandera'] = bandera
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def listar_clientes(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()

    clientes = User.objects.all()

    dic = {
        'clientes': clientes,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/cliente/administrar.html", dic)

def eliminar_clientes(request):
    datos = {}
    id_cli = request.GET.get("id")

    try:
        obj_cli = User.objects.get(id=id_cli)
        obj_cli.is_active = False
        obj_cli.save()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def superusuario_cliente(request):
    datos = {}
    id_cli = request.GET.get("id")
    estado = request.GET.get("estado")

    try:
        obj_cli = User.objects.get(id=id_cli)
        if estado == "true":
            obj_cli.is_superuser = True
        else:
            obj_cli.is_superuser = False

        obj_cli.save()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def activar_clientes(request):
    datos = {}
    id_cli = request.GET.get("id")

    try:
        obj_cli = User.objects.get(id=id_cli)
        obj_cli.is_active = True

        obj_cli.save()

        datos['result'] = "OK"
        datos['id'] = obj_cli.id
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cambio_clave_cliente(request):
    datos = {}
    id_cli = request.POST.get("id_cliente")
    clave_actual = request.POST.get("cli_clave_actual")
    clave_nueva1 = request.POST.get("cli_clave_nueva1")
    clave_nueva2 = request.POST.get("cli_clave_nueva2")

    try:
        obj_cli = User.objects.get(id=id_cli)
        logueo = authenticate(username=obj_cli.username, password=clave_actual)

        if clave_nueva1 != clave_nueva2:
            datos['message'] = gettext("Las contraseñas ingresadas no coinciden.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        if logueo:
            obj_cli.password = make_password(clave_nueva1)
            obj_cli.save()
        else:
            datos['message'] = gettext("La contraseña actual no es la correcta.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        datos['result'] = "OK"
        datos['message'] = gettext("La contraseña ha sido cambiada correctamente.").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Pedidos **************************************************************************************************** *********
def listar_pedidos(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    dic = {
        'notif_stock': productos_bajo_stock,
        'pedidos': pedido.objects.all().order_by("-ped_estado")
    }
    return render(request, "dashboard/pedidos/administrar.html", dic)

def ver_detalle_pedido(request, id_ped):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    if not pedido.objects.filter(ped_id=id_ped).exists():
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'detalle': pedido.objects.get(ped_id=id_ped),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
        'empresa': perfil_empresa.objects.all()[0],
        'items': det_pedido.objects.filter(ped_id=id_ped),
        'ruta': request.META.get('HTTP_REFERER')
    }
    return render(request, "dashboard/pedidos/detalle_pedido.html", dic)

def cambiar_estado_pedidos(request):
    datos = {}
    id_ped = request.POST.get("id_pedido")
    fecha = request.POST.get("id_pedido_fecha")
    estado = request.POST.get("id_estado_pedido")
    guia = request.POST.get("id_gui_pedido")

    try:
        lista = ["pendiente", "enviado", "verificacion", "cancelado"]
        if not estado in lista:
            datos['message'] = gettext("El valor del estado no es el correcto. Actualice la página e inténtelo de nuevo.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        obj_ped = pedido.objects.get(ped_id=id_ped)
        obj_ped.ped_fecha_envio = datetime.datetime.strptime(fecha, "%Y/%m/%d").strftime("%Y-%m-%d %H:%M")

        if obj_ped.ped_estado == estado:
            datos['message'] = gettext("El estado debe ser diferente al que posee actualmente.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        obj_ped.ped_estado = estado
        obj_ped.ped_codigo_guia = guia

        obj_ped.save()

        datos['id'] = obj_ped.ped_id
        datos['cliente'] = obj_ped.cliente.id
        datos['estado'] = obj_ped.ped_estado
        datos['result'] = "OK"
        datos['message'] = gettext("El estado del pedido ha cambiado a").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido cambiar el estado del pedido. COD.: " + str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_deposito(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        ped = pedido.objects.get(ped_id=id_ped)
        datos['imagen'] = ped.ped_imagen_deposito.url
        datos['codigo'] = ped.ped_cod_deposito
        datos['id_p'] = ped.ped_id
        datos['fecha'] = str(ped.ped_fecha_envio).replace("-", "/")
        datos['estado'] = ped.ped_estado
        datos['guia'] = ped.ped_codigo_guia
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_ped_cliente(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        ped = pedido.objects.get(ped_id=id_ped)
        cli = User.objects.get(id=ped.cliente.id)

        imagen = cli.us_avatar
        if not imagen:
            imagen = ""
        else:
            imagen = imagen.url

        datos['imagen'] = imagen
        datos['usuario'] = cli.username
        datos['nombre'] = cli.us_nombre+" "+cli.us_apellidos
        datos['telefono'] = cli.us_telefono_movil
        datos['email'] = cli.email
        datos['fecha'] = str(ped.ped_fecha_envio).replace("-", "/")
        datos['direccion'] = ped.ped_direccion_envio
        datos['ciudad'] = ped.ped_ciudad_envio+" - "+ped.ped_pais_envio
        datos['cod_postal'] = ped.ped_cod_postal_envio
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_pedido(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        obj_ped = pedido.objects.get(ped_id=id_ped)
        obj_ped.delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El pedido ha sido eliminado correctamente. El stock de los productos serán actualizados.").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Transporte ***********************************************************************************************************
def listar_transporte(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'transporte': emp_trasporte.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/transporte.html", dic)

def agregar_transporte(request):
    datos = {}
    nombre = request.POST.get('ajustes_transporte_nombre')
    costo = request.POST.get('ajustes_transporte_costo')
    direccion_url = request.POST.get('ajustes_transporte_url')
    id_trans = request.POST.get('transporte_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    try:
        listaRegistrados = emp_trasporte.objects.all()
        if id_trans:
            listaRegistrados = listaRegistrados.exclude(trans_id=id_trans)

        for l in listaRegistrados:
            if (l.trans_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_trans:
            transporte = emp_trasporte(trans_nombre=nombre, trans_costo=costo, trans_url=direccion_url)
        else:
            transporte = emp_trasporte.objects.get(trans_id=id_trans)
            transporte.trans_nombre = nombre
            transporte.trans_costo = costo
            transporte.trans_url = direccion_url

            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        transporte.save()

        datos['message'] = mensaje
        datos['id'] = transporte.trans_id
        datos['nombre'] = transporte.trans_nombre
        datos['costo'] = str(transporte.trans_costo)
        datos['url'] = transporte.trans_url
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_transporte(request):
    datos = {}
    id_trans = request.GET.get("id")

    try:
        transp = emp_trasporte.objects.get(trans_id=id_trans)
        datos['id'] = transp.trans_id
        datos['nombre'] = transp.trans_nombre
        datos['costo'] = str(transp.trans_costo).replace(",", ".")
        datos['url'] = transp.trans_url
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_transporte(request):
    datos = {}
    id_trans = request.GET.get("id")

    try:
        obj_tran = emp_trasporte.objects.get(trans_id=id_trans)
        obj_tran.delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

#  Impuestos ***********************************************************************************************************
def listar_impuestos(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'impuesto': impuesto.objects.all().order_by("-imp_id"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/impuestos.html", dic)

def agregar_impuestos(request):
    datos = {}
    nombre=request.POST.get('ajustes_impuesto_nombre')
    porcentaje=request.POST.get('ajustes_impuesto_porcentaje')
    id_imp = request.POST.get('ajustes_id_impuesto')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    try:
        listaRegistrados = impuesto.objects.all()
        if id_imp:
            listaRegistrados = listaRegistrados.exclude(imp_id=id_imp)

        for l in listaRegistrados:
            if (l.imp_nombre == nombre):
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_imp:
            if impuesto.objects.all().count() > 0:
                impu = impuesto(imp_nombre=nombre, imp_porcentaje=porcentaje)
            else:
                impu = impuesto(imp_nombre=nombre, imp_porcentaje=porcentaje, imp_actual=True)
        else:
            impu = impuesto.objects.get(imp_id=id_imp)
            impu.imp_id = id_imp
            impu.imp_nombre = nombre
            impu.imp_porcentaje = porcentaje
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        impu.save()

        datos['message'] = mensaje
        datos['id'] = impu.imp_id
        datos['actual'] = impu.imp_actual
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_impuestos(request):
    datos = {}
    id_impuesto = request.GET.get("id")

    try:
        impu = impuesto.objects.get(imp_id=id_impuesto)
        datos['id'] = impu.imp_id
        datos['nombre'] = impu.imp_nombre
        datos['porcentaje'] = impu.imp_porcentaje
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_impuestos(request):
    datos = {}
    id_impuesto = request.GET.get("id")

    try:
        obj_imp = impuesto.objects.get(imp_id=id_impuesto)
        if obj_imp.imp_actual:
            datos['message'] = gettext("No se ha podido eliminar el registro. Es el impuesto usado actualmente").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        obj_imp.delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def impuesto_actual(request):
    datos = {}
    id_imp = request.GET.get("id")

    try:
        obj_nuevo = impuesto.objects.get(imp_id=id_imp)
        obj_actual = impuesto.objects.get(imp_actual=True)

        obj_actual.imp_actual = False
        obj_actual.save()

        obj_nuevo.imp_actual = True
        obj_nuevo.save()

        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Perfil de la empresa *************************************************************************************************
def form_perfil_empresa(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'perfil_empresa': perfil_empresa.objects.all()[0],
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/empresa.html", dic)

def actualizar_perfil_empresa(request):
    datos = {}
    nombre = request.POST.get('ajustes_empresa_nombre')
    email = request.POST.get('ajustes_empresa_email')
    direccion = request.POST.get('ajustes_empresa_direccion')
    telefono = request.POST.get('ajustes_empresa_telefono')
    ruc = request.POST.get('ajustes_empresa_ruc')
    logo = request.FILES.get('id_imagen_logo_nueva')

    bandera_logo = 0

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")

    try:
        empresa = perfil_empresa.objects.all()[0]

        empresa.per_nombre = nombre
        empresa.per_email = email
        empresa.per_direccion = direccion
        empresa.per_telefono = telefono
        empresa.per_ruc = ruc

        if logo:
            if empresa.per_logo:
                os.remove(empresa.per_logo.path)

            empresa.per_logo = logo
            bandera_logo = 1

        empresa.save()

        datos['message'] = mensaje
        datos['bandera'] = bandera_logo
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['bandera'] = bandera_logo
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Notificaciones stock minimo ******************************************************************************************
def listar_productos_bajo_stock(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/notif_stock.html", dic)

# Notificaciones pedidos ***********************************************************************************************
def listar_pedidos_pendientes(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    pedidos_pendientes = verificar_pedidos_pendientes()
    productos_bajo_stock = verificar_stock_minimo()
    dic = {
        'notif_pedidos': pedidos_pendientes,
        'notif_stock': productos_bajo_stock
    }
    return render(request, "dashboard/ajustes/notif_pedidos.html", dic)

# Campaing email *******************************************************************************************************
#Filtrar varios clientes .objects.filter(pro_id__in=[1,4])
def form_agregar_email(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/email/agregar.html", dic)

def form_editar_email(request, id_email):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    email_editar = None
    if id_email:
        if campaign_email.objects.filter(cam_id = id_email).exists():
            email_editar = campaign_email.objects.get(cam_id = id_email)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_email"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'email_editar': email_editar,
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/email/agregar.html", dic)

def listar_email(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'emails': campaign_email.objects.all().order_by("cam_fecha_crea"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/email/administrar.html", dic)

def listar_historial_email(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'historial': historial_campaign_email.objects.all().order_by("his_fecha"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/email/historial.html", dic)

# Tags *********************************************************************************************** *****************
def listar_tags(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'tags': tag.objects.all(),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/tags.html", dic)

@transaction.atomic()
def agregar_tags(request):
    if request.method != "POST":
        return HttpResponseRedirect(reverse("dashboard_app:url_listar_tags"))

    datos = {}
    id_tag = request.POST.get('id_tag_editar')
    str_nombre_tag = request.POST.get('nombre_tag_editar')
    nombre_tag = str(str_nombre_tag).split(",")

    str_etiquetas = request.POST.get('nombre_tag')
    etiquetas = str(str_etiquetas).split(",")

    valores_tag = etiquetas

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")

    transaccion = transaction.savepoint()
    try:
        listaRegistrados = tag.objects.all()
        if id_tag:
            listaRegistrados = listaRegistrados.exclude(tag_id=id_tag)
            valores_tag = nombre_tag
            if len(str_nombre_tag) > 20:
                datos['message'] = gettext("No se ha podido ingresar el registro. Debe ingresar 20 caracteres por cada etiqueta").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")


        for l in listaRegistrados:
            for t in valores_tag:
                if l.tag_nombre == t:
                    datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                    datos['result'] = "X"
                    return HttpResponse(json.dumps(datos), content_type="application/json")

        if not id_tag:
            for t in valores_tag:
                tags = tag(tag_nombre=t)
                tags.save()
        else:
            tags = tag(tag_id=id_tag, tag_nombre=valores_tag[0])
            tags.save()
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        todos = []
        for t in tag.objects.all().order_by("-tag_id"):
            todos.append({'id': t.tag_id, 'nombre': t.tag_nombre})

        if not id_tag:
            bandera = "A"
        else:
            bandera = "E"

        datos['message'] = mensaje
        datos['todos'] = todos
        datos['bandera'] = bandera
        datos['result'] = "OK"

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: " + str(exc.message)).decode("utf-8")
        datos['result'] = "X"

        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_tags(request):
    datos = {}
    id_tag = request.GET.get("id")

    try:
        tags = tag.objects.get(tag_id=int(id_tag))
        datos['id'] = id_tag
        datos['nombre'] = tags.tag_nombre
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_tags(request):
    datos = {}
    id_tag= request.GET.get("id")

    try:
        tag.objects.get(tag_id=id_tag).delete()
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Reviews **************************************************************************************************************
def listar_reviews(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'reviews': review.objects.all().order_by("-rev_fecha"),
        'productos': producto.objects.all().order_by("-pro_id"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/reviews.html", dic)

def eliminar_reviews(request):
    datos = {}
    id_rev = request.GET.get("id")

    try:
        review.objects.get(rev_id=int(id_rev)).delete()

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Imagenes *********************************************************************************************** *************
def listar_imagenes(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'imagenes': imagen_producto.objects.all().order_by("pro_id__pro_id"),
        'productos':producto.objects.all().order_by("-pro_id"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/imagenes.html", dic)

def eliminar_imagenes(request):
    datos = {}
    id_ima = request.GET.get("id")
    nueva_principal = 0

    try:
        img_eliminar = imagen_producto.objects.get(img_id=int(id_ima))
        if img_eliminar.img_es_principal:
            todas = imagen_producto.objects.filter(pro_id=img_eliminar.pro_id).exclude(img_id=img_eliminar.img_id)

            if todas.count() >= 1:
                for prod in todas:
                    prod.img_es_principal = True
                    nueva_principal = prod.img_id
                    prod.save()
                    break

        os.remove(img_eliminar.img_data.path)
        img_eliminar.delete()

        datos['result'] = "OK"
        datos['id_imagen'] = nueva_principal
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['id_imagen'] = nueva_principal
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def cambiar_imagenes(request):
    datos = {}
    id_ima = request.GET.get("id")

    transaccion = transaction.savepoint()
    try:
        img_cambiar_nueva = imagen_producto.objects.get(img_id=int(id_ima))
        if img_cambiar_nueva.img_es_principal:
            raise Exception("Error", "Error")

        img_cambiar_antigua = imagen_producto.objects.get(img_es_principal=True, pro_id=img_cambiar_nueva.pro_id)

        img_cambiar_antigua.img_es_principal = False
        img_cambiar_antigua.save()

        img_cambiar_nueva.img_es_principal = True
        img_cambiar_nueva.save()

        datos['result'] = "OK"
        datos['id_antiguo'] = img_cambiar_antigua.img_id
        datos['id_nuevo'] = img_cambiar_nueva.img_id
        datos['message'] = gettext("La imagen principal ha sido cambiada correctamente").decode("utf-8")

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("Sin cambios que realizar").decode("utf-8")
        datos['result'] = "X"
        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def subir_nueva_imagen(request):
    datos = {}
    imagen = request.FILES.get('id_subir_imagen')
    id_imagen = request.POST.get('imagen_subir_id')

    mensaje = gettext("La imagen ha sido cambiada correctamente").decode("utf-8")

    transaccion = transaction.savepoint()
    try:
        subir_imagen = imagen_producto.objects.get(img_id=int(id_imagen))
        url_anterior = subir_imagen.img_data.path
        subir_imagen.img_data = imagen
        subir_imagen.save()

        os.remove(url_anterior)

        datos['message'] = mensaje
        datos['url'] = subir_imagen.img_data.url
        datos['result'] = "OK"

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de actualizar la imagen. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")


# Ofertas *********************************************************************************************** **************
def listar_ofertas(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'ofertas': oferta.objects.all().order_by("-pro_id__pro_id", "-ofer_fecha_fin"),
        'productos': producto.objects.all().order_by("-pro_id"),
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/productos/ofertas.html", dic)

def agregar_ofertas(request):
    datos = {}
    str_fecha_inicio = request.POST.get('producto_fecha_inicio_crud')
    str_fecha_fin = request.POST.get('producto_fecha_fin_crud')
    porcentaje = request.POST.get('producto_precio_oferta_crud')
    id_oferta = request.POST.get('oferta_id')
    id_producto = request.POST.get('producto_oferta_id')
    id_producto_crud = request.POST.get('id_producto_oferta_crud')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")

    #Formato de fecha correcto
    fecha_inicio =  datetime.datetime.strptime(str_fecha_inicio, "%Y/%m/%d").strftime("%Y-%m-%d")
    fecha_fin = datetime.datetime.strptime(str_fecha_fin, "%Y/%m/%d").strftime("%Y-%m-%d")

    try:
        if id_producto:
            if not producto.objects.filter(pro_id=id_producto).exists():
                raise Exception("Error", "No existe el producto")

        if id_producto_crud:
            if not producto.objects.filter(pro_id=id_producto_crud).exists():
                raise Exception("Error", "No existe el producto")

        if not id_oferta:
            # Verifica si existe una oferta activa para el producto
            #o = oferta.objects.filter(pro_id_id=id_producto_crud).order_by("-ofer_fecha_fin")[0]
            #if datetime.datetime.strptime(str_fecha_inicio, "%Y/%m/%d") <= datetime.datetime.strptime(o.ofer_fecha_fin, "%Y-%m-%d"):
                #datos['message'] = gettext("Existe una oferta actualmente para el producto "+o.pro_id.pro_nombre).decode("utf-8")
                #datos['result'] = "X"
                #return HttpResponse(json.dumps(datos), content_type="application/json")

            ofer = oferta(ofer_fecha_inicio=fecha_inicio, ofer_fecha_fin=fecha_fin, ofer_porcentaje=porcentaje, pro_id_id=id_producto_crud)
            ofer.save()
        else:
            ofer = oferta.objects.get(ofer_id=id_oferta)
            ofer.ofer_fecha_inicio = fecha_inicio
            ofer.ofer_fecha_fin = fecha_fin
            ofer.ofer_porcentaje = porcentaje

            ofer.save()
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datos['message'] = mensaje
        datos['id_producto'] = ofer.pro_id.pro_id
        datos['nombre_producto'] = ofer.pro_id.pro_nombre
        datos['fecha_i'] = ofer.ofer_fecha_inicio
        datos['fecha_f'] = ofer.ofer_fecha_fin
        datos['porcentaje'] = ofer.ofer_porcentaje
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_ofertas(request):
    datos = {}
    id_oferta = request.GET.get("id")

    try:
        ofer = oferta.objects.get(ofer_id=int(id_oferta))

        # Formato de fecha correcto
        fecha_inicio = str(ofer.ofer_fecha_inicio).replace("-", "/")
        fecha_fin = str(ofer.ofer_fecha_fin).replace("-", "/")
        porcen = str(ofer.ofer_porcentaje).replace(",", ".")

        datos['id'] = id_oferta
        datos['fecha_i'] = fecha_inicio
        datos['fecha_f'] = fecha_fin
        datos['porcentaje'] = porcen
        datos['producto_id'] = ofer.pro_id.pro_id
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_ofertas(request):
    datos = {}
    id_ofer = request.GET.get("id")

    try:
        oferta.objects.get(ofer_id=id_ofer).delete()
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Reportes ************************************************************************************************************
def reporte_compras(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
    }
    return render(request, "dashboard/reportes/compras.html", dic)

def reporte_ventas(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    productos_bajo_stock = verificar_stock_minimo()
    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_stock': productos_bajo_stock,
        'notif_pedidos': pedidos_pendientes,
    }
    return render(request, "dashboard/reportes/ventas.html", dic)

# Funciones globales ***************************************************************************************************
# Verificar stock minimo de producto
def verificar_stock_minimo():
    productos_bajo_stock = producto.objects.filter(pro_stock_minimo__gte=F("pro_stock")).prefetch_related(
        Prefetch(
            "imagen_producto_set",
            queryset=imagen_producto.objects.filter(img_es_principal=True),
            to_attr="pro_imagen_principal"
        )
    ).order_by("pro_stock")
    return  productos_bajo_stock

# Verificar pedidos pendientes
def verificar_pedidos_pendientes():
    pedidos_pendientes = pedido.objects.filter(Q(ped_estado='pendiente')|Q(ped_estado='verificacion')).order_by("-ped_estado")
    return  pedidos_pendientes

#Cargar todos los tags
def cargar_todos_tags(request):
    datos = []
    tags = tag.objects.all()
    for t in tags:
        datos.append({'value': t.tag_id, 'label': t.tag_nombre})

    return HttpResponse(json.dumps(datos), content_type="application/json")

#Eliminar archivos de la carpeta media (fisica)
def borrar_archivos_media(ruta):
    estado = True
    try:
        os.remove(ruta)
    except:
        estado = False

    return estado

#Filtrar nombre de producto en el buscador del datatable
def filtrar_nombre_producto(request):
    datos = {}
    id_prod = request.GET.get("id")

    try:
        filtrar_prod = producto.objects.get(pro_id=id_prod)
        nombre = filtrar_prod.pro_nombre
    except Exception as exc:
        nombre = ""

    datos['nombre'] = nombre

    return HttpResponse(json.dumps(datos), content_type="application/json")

#Devuelve todos los productos (id-nombre)
def json_productos(request):
    datos = []
    prod = producto.objects.all().order_by("-pro_id")

    for p in prod:
        datos.append({'id': p.pro_id, 'text': p.pro_nombre})

    return HttpResponse(json.dumps(datos), content_type="application/json")