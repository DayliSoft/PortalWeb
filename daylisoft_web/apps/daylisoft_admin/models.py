from django.db import models
from django.conf import settings
import datetime

class perfil_empresa(models.Model):
    per_id = models.AutoField(primary_key=True)
    per_nombre = models.CharField(max_length=100)
    per_email = models.CharField(max_length=100)
    per_direccion = models.CharField(max_length=250)
    per_telefono = models.CharField(max_length=30)
    per_celular = models.CharField(max_length=30)
    per_logo= models.ImageField(upload_to='logos_empresas')
    per_ruc = models.CharField(max_length=20)

    def __unicode__(self):
        return self.per_nombre

    class Meta:
        verbose_name_plural=u'Perfiles'
        app_label = 'daylisoft_admin'

class impuesto(models.Model):
    imp_id = models.AutoField(primary_key=True)
    imp_nombre = models.CharField(max_length=100)
    imp_porcentaje = models.FloatField(blank=False, null=False, default=0)
    imp_actual = models.BooleanField(default=False)

    def __unicode__(self):
        return self.imp_nombre

    class Meta:
        verbose_name_plural=u'Impuestos'
        app_label = 'daylisoft_admin'

class campaign_email(models.Model):
    cam_id = models.AutoField(primary_key=True)
    cam_nombre = models.CharField(max_length=100)
    cam_asunto = models.CharField(max_length=100)
    cam_cuerpo_email = models.TextField()
    cam_usuario_crea = models.ForeignKey(settings.AUTH_USER_MODEL)
    cam_fecha_crea = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.cam_nombre

    class Meta:
        verbose_name_plural=u'Email'
        app_label = 'daylisoft_admin'

class historial_campaign_email(models.Model):
    cam_id = models.ForeignKey(campaign_email)
    his_usuario_envia = models.ForeignKey(settings.AUTH_USER_MODEL)
    his_fecha = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.cam_id.cam_nombre

    class Meta:
        verbose_name_plural=u'Historial_Email'
        app_label = 'daylisoft_admin'