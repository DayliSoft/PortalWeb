from django.conf.urls import patterns, url
from .views import *
urlpatterns = patterns('',
	url(r'^categoria/(\w+)/$', blog_categorias, name='blogCategoria'),
	url(r'^categoria/(?P<cat>\w+)/(?P<blog>\d+)/$', blog_detalles, name='blogDetalle'),
	url(r'^comentarios/$', cargar_comentarios, name='cargarComentarios'),
	url(r'^agregar_comentario/$', agregar_comentario, name='agregar_comentarios'),
)