#-*- coding: utf-8 -*-
import json
from django.views.decorators.csrf import *
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect
from .models import *
from django.contrib import messages
from apps.portal.models import redesSociales
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def blog_categorias(request, cat):
    try:
        context={}
        page = request.GET.get('page')
        if(cat=='ver'):
            if(categoriaBlog.objects.all().exists()):
                categoria = categoriaBlog.objects.get(cat_nombre=categoriaBlog.objects.all()[0].cat_nombre)
            else:
                messages.add_message(request, messages.SUCCESS, 'No existen categorias ingresadas')
                return  redirect('/')
        else:
            categoria = categoriaBlog.objects.get(cat_nombre=cat)
        categorias = categoriaBlog.objects.all().exclude(cat_nombre=categoria.cat_nombre)
        entradas = entrada.objects.filter(ent_categoria=categoria).order_by('-ent_fecha')
        context['categoria'] = categoria
        context['categorias'] = categorias
        paginator = Paginator(entradas, 5)
        context['blogs'] = entrada.objects.all().order_by('-ent_fecha')
        try:
            context['blogCategoria'] = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            context['blogCategoria'] = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            context['blogCategoria'] = paginator.page(paginator.num_pages)
        context['numeroPaginas']=range(1, paginator.num_pages)
        blogsU=entrada.objects.all().order_by('-ent_fecha')
        datos= []
        for i in entradas:
            for j in i.ent_tags.all():
                if j.tag_nombre not in datos:
                    datos.append(j.tag_nombre)
        context['etiquetasBlog']=datos
        blogsUI=blogsU[0:8]
        imgs= []
        for i in blogsUI:
            imgs.append(i.images.all())
        context['imagenesBlog']=imgs
        return render_to_response('blog/list_blog.html', context, context_instance=RequestContext(request))
    except categoriaBlog.DoesNotExist:
        messages.add_message(request, messages.SUCCESS, 'Por el momento no exiten blog registrados')
        return  redirect('/')

def blog_detalles(request, cat, blog):
    try:
        context={}
        blog=entrada.objects.get(ent_id=blog)
        textos=entradasTextos.objects.filter(etex_entrada_id=blog.ent_id).order_by('-etex_fecha')
        blogsU=entrada.objects.all().order_by('-ent_fecha')
        context['redes']=redesSociales.objects.all()
        context['seleccionBlog']=blog
        context['ultimosBlog']=blogsU
        print(textos.all())
        page = request.GET.get('page')
        paginator = Paginator(textos, 1)
        context['blogTextos'] = paginator.page(page)
        return render_to_response('blog/detail_blog.html', context, context_instance=RequestContext(request))
    except PageNotAnInteger:
        context['blogTextos'] = paginator.page(1)
        return render_to_response('blog/detail_blog.html', context, context_instance=RequestContext(request))
    except EmptyPage:
        context['blogTextos'] = paginator.page(paginator.num_pages)
        return render_to_response('blog/detail_blog.html', context, context_instance=RequestContext(request))
    except Exception as e:
        print(e)
        print('No existe ese post en esa categoria')
        return blog_categorias(request, cat)

@csrf_exempt
def cargar_comentarios(request):
    data = []
    try:
        cmt = comentarios.objects.filter(com_entrada=request.POST['identificador']).order_by('com_id')
        for p in cmt:
            _cmt_padre="0"
            try:
                _cmt_padre = p.com_padre.com_id
            except Exception as error:
                _cmt_padre="0"
            try:
            	if(p.com_usuario is None):
            		nombre=p.com_nombre
            		imagen='/media/users/anonimo.gif'
            	else:
            		nombre=p.com_usuario.username
            		imagen=p.com_usuario.us_avatar.url
            except Exception as error:
            	print error
            	imagen='/media/users/anonimo.gif'
            fecha=p.com_fecha
            data.append({'com_id': p.com_id, 'com_texto': p.com_texto, 'com_padre': _cmt_padre, 'com_usuario':nombre,
            	'com_avatar':imagen, 'com_fecha':(str(fecha.day)+'/'+str(fecha.month)+'/'+str(fecha.year)+' '+
            										str(fecha.hour)+':'+str(fecha.minute))})
    except Exception as error:
        print(error.message)
        data = ''
    return HttpResponse(json.dumps(data), content_type='application/json')

@csrf_exempt
def agregar_comentario(request):
    data = []
    if request.method == "GET":
    	if request.user.is_authenticated():
    		tema = request.GET["tema"]
    		mensaje = request.GET["mensaje"]
    		entradaC = request.GET["entrada"]
    		if 'id_comentario' not in request.GET:
	    		cSave=comentarios(com_usuario=request.user, com_texto=mensaje, com_tema=tema, com_entrada=entrada.objects.get(ent_id=entradaC))
    			cSave.save()
    		else:
    			cSave=comentarios(com_usuario=request.user, com_texto=mensaje, com_tema=tema,
    				com_entrada=entrada.objects.get(ent_id=entradaC), com_padre=comentarios.objects.get(com_id=request.GET["id_comentario"]))
    			cSave.save()
    	else:
    		nombre = request.GET["nombre"]
    		email = request.GET["email"]
    		tema = request.GET["tema"]
    		mensaje = request.GET["mensaje"]
    		entradaC = request.GET["entrada"]
    		if 'id_comentario' not in request.GET:
    			cSave=comentarios(com_nombre=nombre, com_email=email, com_tema=tema, com_texto=mensaje, com_entrada=entrada.objects.get(ent_id=entradaC))
    			cSave.save()
    		else:
    			cSave=comentarios(com_nombre=nombre, com_email=email, com_tema=tema, com_texto=mensaje,
    				com_entrada=entrada.objects.get(ent_id=entradaC), com_padre=comentarios.objects.get(com_id=request.GET["id_comentario"]))
    			cSave.save()
    	dic = {
            'result': 'OK'
        }
    	messages.add_message(request, messages.SUCCESS, 'Comentario registrado')
    return HttpResponse(json.dumps(dic), content_type='application/json')