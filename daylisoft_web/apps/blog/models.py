from django.db import models
from django.conf import settings
from apps.users.models import User
from PIL import Image

# from django.contrib.auth.models import User, Group

class categoriaBlog(models.Model):
	cat_id = models.AutoField(primary_key=True)
	cat_nombre = models.CharField('Nombre', max_length=40)

	def __unicode__(self):
		return self.cat_nombre

class tipoArchivo(models.Model):
	tarc_nombre=models.CharField('Nombre', max_length=40)
	tarc_extension=models.CharField('Extension', max_length=10)
	def __unicode__(self):
		return self.tarc_nombre

class entrada(models.Model):
	ent_id = models.AutoField(primary_key=True)
	ent_usuario = models.ForeignKey(User)
	ent_titulo = models.CharField('Titulo', max_length=40, blank=True)
	ent_descripcion = models.CharField('Descripcion Breve', max_length=40, blank=True)
	ent_texto=models.TextField('Texto')
	ent_fecha = models.DateTimeField(auto_now_add=True)
	ent_estado = models.BooleanField(default=True)
	ent_categoria = models.ForeignKey(categoriaBlog)
	ent_tags = models.ManyToManyField('tag', related_name='ent_tags')

	def __unicode__(self):
		return self.ent_titulo

class tag(models.Model):
	tag_id = models.AutoField(primary_key=True)
	tag_nombre = models.CharField('Nombre', max_length=8)

	def __unicode__(self):
		return self.tag_nombre

	def __str__(self):
		return self.tag_nombre

	def get_short_name(self):
		return self.tag_nombre

class comentarios(models.Model):
	com_id = models.AutoField(primary_key=True)
	com_usuario = models.ForeignKey(User, null=True, blank=True)
	com_nombre = models.CharField(max_length=100, null=True, blank=True)
	com_email = models.CharField(max_length=100, null=True, blank=True)
	com_tema = models.CharField(max_length=100)
	com_texto = models.CharField(max_length=100)
	com_entrada= models.ForeignKey(entrada, related_name='comentarios')
	com_fecha = models.DateTimeField(auto_now_add=True)
	com_padre = models.ForeignKey('self', null=True, blank=True)

	def __unicode__(self):
		return self.com_texto+'-'+str(self.com_padre)

class Imagenes(models.Model):
	ima_id=models.AutoField(primary_key=True)
	img_entrada = models.ForeignKey(entrada, related_name='images')
	img_imagen = models.ImageField('Imagen', upload_to = 'blog')
	# img_imagen_orginial = models.ImageField('ImagenOriginal', upload_to = 'blog')

	# def save(self):
	# 	if not self.img_imagen:
	# 		return

	# 	super(Imagenes, self).save()
	# 	img_imagen = Image.open(self.img_imagen)
	# 	ancho,alto = img_imagen.size
	# 	ratio_height = (50*alto)/ancho
	# 	size = (270, 200)
	# 	img_imagen = img_imagen.resize(size, Image.ANTIALIAS)
	# 	img_imagen.save(self.img_imagen.path)
	# def __unicode__(self):
	# 	return self.img_imagen.url


class entradasTextos(models.Model):
	etex_id = models.AutoField(primary_key=True)
	etex_entrada = models.ForeignKey(entrada, related_name='textos')
	etex_fecha = models.DateTimeField(auto_now_add=True)
	etex_texto = models.TextField('Texto')

	def __unicode__(self):
		return self.etex_entrada.ent_titulo