from django.contrib import admin
from .models import *
from django.forms import ModelForm
from django.forms import ModelForm, TextInput
from suit.widgets import LinkedSelect
from suit.widgets import SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget

from django.contrib.admin import ModelAdmin
from suit_ckeditor.widgets import CKEditorWidget

# Register your models here.
admin.site.register(categoriaBlog)
admin.site.register(tag)
# admin.site.register(entrada)
admin.site.register(comentarios)
admin.site.register(tipoArchivo)

class ImagenesInline(admin.TabularInline):
    model = Imagenes
    extra = 3

class PropertyAdmin(admin.ModelAdmin):
    inlines = [ ImagenesInline, ]

admin.site.register(Imagenes)
# admin.site.register(entradasTextos)

class PageForm(ModelForm):
    class Meta:
        widgets = {
            'ent_texto': CKEditorWidget(editor_options={'startupFocus': True})
        }

class PageAdmin(ModelAdmin):
    form = PageForm
    fieldsets = [
    ('Blog', {
        'fields': ['ent_usuario', 'ent_titulo', 'ent_descripcion', 'ent_categoria', 'ent_tags'],
    }),
      ('Ingrese el blog', {'classes': ('full-width',), 'fields': ('ent_texto',)}),

    ]

admin.site.register(entrada, PageAdmin)

class PageFormTextos(ModelForm):
    class Meta:
        widgets = {
            'etex_texto': CKEditorWidget(editor_options={'startupFocus': True})
        }

class PageAdminTextos(ModelAdmin):
    form = PageFormTextos
    fieldsets = [
    ('Textos para el blog', {
        'fields': ['etex_entrada'],
    }),
      ('Ingrese el texto', {'classes': ('full-width',), 'fields': ('etex_texto',)}),

    ]

admin.site.register(entradasTextos, PageAdminTextos)