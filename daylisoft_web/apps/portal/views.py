from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect
from apps.blog.models import *
from .models import *
from django.contrib import messages
from django.core.mail import send_mail




# Create your views here.
def inicio(request):
	context={}
	context['redes']=redesSociales.objects.all()
	return render_to_response('index.html', context, context_instance=RequestContext(request))

def quienesSomos(request):
	context={}
	context['acerca'] = portal.objects.get(por_etiqueta='acerca')
	context['mision'] = portal.objects.get(por_etiqueta='mision')
	context['vision'] = portal.objects.get(por_etiqueta='vision')
	context['fundador'] = portal.objects.get(por_etiqueta='fundador')
	context['cofundador'] = portal.objects.get(por_etiqueta='co-fundador')
	context['administrador'] = portal.objects.get(por_etiqueta='gerente')
	context['redes']=redesSociales.objects.all()
	return render_to_response('portal/quienesSomos.html', context, context_instance=RequestContext(request))

def servicios(request):
	context={}
	context['redes']=redesSociales.objects.all()
	return render_to_response('portal/servicios.html', context, context_instance=RequestContext(request))

def equipo(request):
	context={}
	context['fundador'] = portal.objects.get(por_etiqueta='fundador')
	context['cofundador'] = portal.objects.get(por_etiqueta='co-fundador')
	context['administrador'] = portal.objects.get(por_etiqueta='gerente')
	context['redes']=redesSociales.objects.all()
	return render_to_response('portal/equipo.html', context, context_instance=RequestContext(request))

def galeriaV(request):
	context={}
	context['galeria'] = galeria.objects.all()
	context['redes']=redesSociales.objects.all()
	return render_to_response('portal/galeria.html', context, context_instance=RequestContext(request))


def contacto(request):
	context={}
	context['galeria'] = galeria.objects.all()
	context['redes']=redesSociales.objects.all()
	return render_to_response('portal/contactenos.html', context, context_instance=RequestContext(request))


def enviar_correo(request):
    data = []
    if request.method == "POST":
        if request.user.is_authenticated():
            tema = request.POST["tema"]
            mensaje = request.POST["mensaje"]
            nombre=request.user
            email = request.user.email
            print(email)
        else:
            nombre = request.POST["nombre"]
            email = request.POST["email"]
            tema = request.POST["tema"]
            mensaje = request.POST["mensaje"]
        email_subject = tema
        email_body = mensaje
        send_mail(email_subject, email_body, email,['daylisoft@gmail.com'], fail_silently=False)
        messages.add_message(request, messages.SUCCESS, 'Su Mensaje se ha enviado')
    return redirect('/portal/contacto/')


def serviciosAdicionales(request):
    context={}
    context['redes']=redesSociales.objects.all()
    return render_to_response('portal/otros_servicios.html', context, context_instance=RequestContext(request))