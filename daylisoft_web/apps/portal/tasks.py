# -*- coding: utf-8 -*-
#from celery import shared_task
from apps.portal.models import *
import openpyxl
from django.contrib import messages
from apps.blog.models import *
from apps.portal.views import inicio

def restaurar(request):
	documento=openpyxl.load_workbook('restaurar.xlsx')
	# CATEGORIAS
	bc=categoriaBlog.objects.all()
	if bc.exists():
		bc.delete()
	hoja=documento.get_sheet_by_name('categorias')
	rango=hoja['A2':'A5']
	for fila in rango:
		categoria=categoriaBlog(cat_nombre=fila[0].value)
		categoria.save()
	print ("Se guardaron las categorias por defecto")

    # PORTAL
	bc=portal.objects.all()
	if bc.exists():
		bc.delete()
	hoja=documento.get_sheet_by_name('portal')
	rango=hoja['A2':'C7']
	for fila in rango:
		portalq=portal(por_etiqueta=fila[0].value, por_texto=fila[1].value, por_imagen=fila[2].value)
		portalq.save()

    # Galeria
	bc=galeria.objects.all()
	if bc.exists():
		bc.delete()
	hoja=documento.get_sheet_by_name('galeria')
	rango=hoja['A2':'B21']
	for fila in rango:
		galeriaq=galeria(gal_etiqueta=fila[0].value, gal_imagen=fila[1].value)
		galeriaq.save()

    # Redes
	bc=redesSociales.objects.all()
	if bc.exists():
		bc.delete()
	hoja=documento.get_sheet_by_name('redes')
	rango=hoja['A2':'D7']
	for fila in rango:
		redes=redesSociales(red_nombre=fila[0].value, red_usuario=fila[1].value,
						red_link=fila[2].value, red_imagen=fila[3].value)
		redes.save()

	print ("Se guardaron la configuracion del portal por defecto")
	return inicio(request)