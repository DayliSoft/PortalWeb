from django.conf.urls import patterns, url
from apps.portal.views import *
from apps.portal.tasks import *
urlpatterns = patterns('',
	url(r'^$', inicio, name='index'),
	url(r'^portal/quienesSomos/$', quienesSomos, name='quienesSomos'),
	url(r'^restaurar/$', restaurar, name='restaurar'),
    url(r'^portal/servicios/$', servicios, name='servicios'),
    url(r'^portal/equipo/$', equipo, name='equipo'),
    url(r'^portal/galeria/$', galeriaV, name='galeria'),
    url(r'^portal/contacto/$', contacto, name='contacto'),
    url(r'^portal/enviarmail/$', enviar_correo, name='enviarmail'),
    url(r'^portal/serviciosAdicionales/$', serviciosAdicionales, name='serviciosAdicionales'),
)