# -*- coding: utf-8 -*-
from PIL import Image
from django.db import models


# Create your models here.
class portal(models.Model):
	"""docstring for ClassName"""
	por_etiqueta = models.CharField('Etiqueta',max_length=40, blank=True)
	por_texto = models.CharField('Texto',max_length=2000, blank=True)
	por_imagen = models.ImageField('Imagen',upload_to = 'portal', null=True,blank=True)

	def __unicode__(self):
		return self.por_etiqueta
	class Meta:
		verbose_name_plural=u'Textos para el Portal'

class galeria(models.Model):
    gal_etiqueta = models.CharField('Etiqueta',max_length=40, blank=True)
    gal_imagen = models.ImageField('Imagen',upload_to = 'portal/galeria', null=True,blank=True)

    def __unicode__(self):
        return self.gal_etiqueta
    class Meta:
        verbose_name_plural=u'Galeria del Portal'

class redesSociales(models.Model):
    red_id = models.AutoField(primary_key=True)
    red_nombre=models.CharField('Nombre', max_length=50, blank=True)
    red_usuario = models.CharField('Usuario',max_length=200, blank=True)
    red_link= models.CharField('Link',max_length=300, blank=True)
    red_imagen = models.ImageField('Imagen',upload_to = 'portal/redes', null=True,blank=True)

    def __unicode__(self):
        return self.red_nombre
    class Meta:
        verbose_name_plural=u'Redes sociales'

    # def save(self):
    #     if not self.red_imagen:
    #         return

    #     super(redesSociales, self).save()
    #     red_imagen = Image.open(self.red_imagen)
    #     ancho, alto = red_imagen.size
    #     ratio_height = (50*alto)/ancho
    #     size = (160, 42)
    #     red_imagen = red_imagen.resize(size, Image.ANTIALIAS)
    #     red_imagen.save(self.red_imagen.path)