from django import template
from django import template
from django.template.defaultfilters import stringfilter
from ...blog.models import *
register = template.Library()


@register.filter()
def twitter_date(value):
    import datetime
    split_date = value.split()
    del split_date[0], split_date[-2]
    value = ' '.join(split_date)  # Fri Nov 07 17:57:59 +0000 2014 is the format
    return datetime.datetime.strptime(value, '%b %d %H:%M:%S %Y')


@register.filter()
def urlize_tweet_text(tweet):
    """ Turn #hashtag and @username in status text to Twitter hyperlinks,
        similar to the ``urlize()`` function in Django.
    """
    try:
        from urllib import quote
    except ImportError:
        from urllib.parse import quote
    hashtag_url = '<a href="https://twitter.com/search?q=%%23%s" target="_blank">#%s</a>'
    user_url = '<a href="https://twitter.com/%s" target="_blank">@%s</a>'
    text = tweet.text
    for hash in tweet.hashtags:
        text = text.replace('#%s' % hash.text, hashtag_url % (quote(hash.text.encode("utf-8")), hash.text))
    for mention in tweet.user_mentions:
        text = text.replace('@%s' % mention.screen_name, user_url % (quote(mention.screen_name), mention.screen_name))
    return text

@register.filter()
def expand_tweet_urls(tweet):
    """ Replace shortened URLs with long URLs in the twitter status
        Should be used before urlize_tweet
    """
    text = tweet.text
    urls = tweet.urls
    for url in urls:
        text = text.replace(url.url, '<a href="%s" target="_blank">%s</a>' % (url.expanded_url, url.url))
    tweet.SetText(text)
    return tweet



@register.filter()
def in_comentario(cmt):
    comentariosQuery=comentarios.objects.filter(com_padre=cmt).exclude(com_padre__isnull = True)
    return comentariosQuery
register.filter('in_comentario', in_comentario)

@register.filter()
def contar_comentarios(comentario):
    contar=comentarios.objects.filter(com_padre=comentario).exclude(com_padre__isnull = True)
    print(contar,'llegas')
    # return things.filter(category=category)
    return contar.count()
register.filter('contar_comentarios', contar_comentarios)


@register.filter()
def ultimosComentarios(num):
    entradaQuery=entrada.objects.all().order_by('-ent_fecha')[0:3]
    return entradaQuery
register.filter('ultimosComentarios', ultimosComentarios)


import twitter
def get_tweets(tw):
     # # api = twitter.Api(consumer_key='4MIXuv6O59yTxHP8v9yfmMljW',
     # #                   consumer_secret='1JTHOI6wPW7M8WE2Iu0s2ErP2bN21lBZtOdPFTk6dLbtKw2ZxD',
     # #                   access_token_key='730962825909313536-55m0n3fjvKqi2e1thJ9oOTJ5wEUAbMk',
     # #                   access_token_secret='F21FIqZ0S7AP8m2FxYwZgpDvY6vocvEun64hQ9EuYUC27')
     # auth = twitter.OAuth('730962825909313536', 'F21FIqZ0S7AP8m2FxYwZgpDvY6vocvEun64hQ9EuYUC27',
     #                      '1JTHOI6wPW7M8WE2Iu0s2ErP2bN21lBZtOdPFTk6dLbtKw2ZxD', '4MIXuv6O59yTxHP8v9yfmMljW')
     # print auth
     # api = twitter.Twitter(auth=auth)
     # print 'api'
     # imagen=api.GetUserTimeline(screen_name='daylisoft_portal')
     # print imagen[1]
     # return imagen  # includes entities
     return []
register.filter('get_tweets', get_tweets)
