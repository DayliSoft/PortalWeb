from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(portal)
admin.site.register(galeria)
admin.site.register(redesSociales)