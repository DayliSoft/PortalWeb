from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
    url(r'^login/$', userlogin, name="login"),
    url(r'^salir/$', LogOut, name = 'logout'),
    url(r'^registro/$', registro, name = 'registro'),
    url(r'^confirm/(?P<activation_key>\w+)/', register_confirm),
 	url(r'^resetear/$', resetear, name = 'resetear'),   
 	url(r'^reset/(?P<code>\w+)/', reset_pass),
 	url(r'^reset-confirm/$', reset_confirm),
)