# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect,get_object_or_404,render_to_response
from django.contrib.auth import logout
from .models import User, UserProfile
from .functions import LogIn
from django.core.mail import send_mail
from django.template.loader import render_to_string
import hashlib, datetime, random
from django.utils import timezone
from django.http import HttpResponseRedirect, HttpResponse 
from django.http import JsonResponse
from apps.daylisoft_admin.models import perfil_empresa

# Create your views here.
def userlogin(request):
    if request.method == "POST":
        user = request.POST['name']
        cont = request.POST['password']
        dic = LogIn(request, user,cont)
        # resp = json.loads(dic)
        print dic['msg']
        if dic['msg'] is 'ok':
            response = JsonResponse(dic)
            return HttpResponse(response.content)
        else:
            response = JsonResponse(dic)
            return HttpResponse(response.content)
    else:
        return render(request, 'index.html')

def registro(request):
    if request.method == "POST":
        try:
            User.objects.create_user(username = request.POST['username'],
                     email = request.POST['email'], 
                     us_nombre = request.POST['nombre'],
                     us_apellidos = request.POST['apellido'],
                     us_telefono_movil = request.POST['telf'],
                     password = request.POST['cont'])

            username = request.POST['username']
            email = request.POST['email']

            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
            activation_key = hashlib.sha1(salt+email).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            #Obtener el nombre de usuario
            user=User.objects.get(username=username)

            # Crear el perfil del usuario                                                                                                                                 
            new_profile = UserProfile(user=user, activation_key=activation_key,key_expires=key_expires)
            new_profile.save()

            # contexto

            ctx = {
                'usuario' : user,
                'code' : activation_key
            }

            asunto = 'Bienvenido a DayliSoft'
            msg_plain = render_to_string('template_emails/registro_usuario/index.txt', ctx)
            msg_html = render_to_string('template_emails/registro_usuario/index.html', ctx)
            find_empresa = perfil_empresa.objects.get(per_ruc='0791786371001')

            # Enviar un email de confirmación
            send_mail(
                asunto,
                msg_plain,
                find_empresa.per_email,
                [email],
                html_message=msg_html,
            )

            dic = {
                'result': 'OK'
            }

        except Exception, e:
            print e
            dic = {
                'result': 'ERR'
            }

        response = JsonResponse(dic)
        return HttpResponse(response.content)

    return render(request, 'users/registro.html')

def LogOut(request):
    logout(request)
    return redirect('/')

def register_confirm(request, activation_key):  
    # Verifica que el usuario ya está logeado
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # Verifica que el token de activación sea válido y sino retorna un 404
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    # verifica si el token de activación ha expirado y si es así renderiza el html de registro expirado
    if user_profile.key_expires < timezone.now():
        return render_to_response('users/registro_expirado.html')
    # Si el token no ha expirado, se activa el usuario y se muestra el html de confirmación
    user = user_profile.user
    user.is_active = True
    user.save()
    return render_to_response('users/registro_ok.html')

# Reseteo de la contraseña

def resetear(request):
    if request.method == "POST":
        dic = {}
        try:
            emailUs = request.POST['email']
            user=User.objects.filter(email=emailUs)
            if user.exists():
                salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
                activation_key = hashlib.sha1(salt+emailUs).hexdigest()
                key_expires = datetime.datetime.today() + datetime.timedelta(2)

                find_user=User.objects.get(email=emailUs)
                # Crear el perfil del usuario                                                                                                                                 
                new_profile = UserProfile(user=find_user, activation_key=activation_key,key_expires=key_expires)
                new_profile.save()

                # Enviar un email de confirmación
                # contexto

                ctx = {
                    'usuario' : user,
                    'code' : activation_key
                }

                asunto = 'Restablecer contraseña'
                msg_plain = render_to_string('template_emails/resetear_cont/index.txt', ctx)
                msg_html = render_to_string('template_emails/resetear_cont/index.html', ctx)
                find_empresa = perfil_empresa.objects.get(per_ruc='0791786371001')

                # Enviar un email de confirmación
                send_mail(
                    asunto,
                    msg_plain,
                    find_empresa.per_email,
                    [emailUs],
                    html_message=msg_html,
                )
                dic = {'msg':'ok'}
            else:
                dic = {'msg':'err'}
                print 'No existe un usuario con ese mail'
        except Exception, e:
            dic = {'msg':e}
            print e
    response = JsonResponse(dic)
    return HttpResponse(response.content)

def reset_pass(request, code):
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # Verifica que el token de activación sea válido y sino retorna un 404
    user_profile = get_object_or_404(UserProfile, activation_key=code)

    # verifica si el token de activación ha expirado y si es así renderiza el html de registro expirado
    if user_profile.key_expires < timezone.now():
        return render_to_response('users/reset_expirado.html')

    dic = {
        'code' : code
    }
    return render_to_response('users/reset.html',dic)

def reset_confirm(request):

    if request.is_ajax():

        try:
            print request.POST
            find_user_profile = UserProfile.objects.get(activation_key = request.POST['code'])
            find_user = find_user_profile.user
            find_user.set_password(request.POST['contrasenia'])
            find_user.save()

            dic = {'msg' : 'ok'}

            find_user_profile.delete()

        except Exception, e:
            print e
            dic = {'msg' : 'err'}

        response = JsonResponse(dic)
        return HttpResponse(response.content)

    else:
        HttpResponseRedirect('/')