jQuery(document).ready(function() {
    "use strict";
    jQuery('.fullscreen_slider').show().revolution({
        delay: 5000,
        startwidth: 1170,
        startheight: 765,
        fullWidth: "off",
        fullScreen: "on",
        navigationType: "bullet",
        fullScreenOffsetContainer: ".main_header",
        fullScreenOffset: ""
    });
});

$('#form_login').on("submit", function() {
    var hhost = getHost();
    $('#noCuentaReg').removeClass('show');
    $('#cuentaNoActiva').removeClass('show');
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "/user/login/",
        dataType: 'json',
        data: $(this).serialize(),
        success: function(response) {
            if (response.msg === 'ok') {
                location.href = hhost;
            } else if (response.msg === 'noActivo') {
                $('#cuentaNoActiva').addClass('show');
            } else {
                $('#noCuentaReg').addClass('show');
            }
        },

        error: function() {
            console.log('Error');
        }
    });
});

function getHost(){
    host = document.location.pathname;
    return host;
}

function modalReceteo() {
    $('#modalReg').modal('hide');
    $('#modalRes').modal('show');
}

function modalReg() {
    $('#modalReg').modal('show');
}

function modalRegistro() {
    $('#modalReg').modal('hide');
    $('#modalRegistro').modal('show');
}

$('#form_registro').on("submit", function() {
    event.preventDefault();
    $('#espera').addClass("show");
    $.ajax({
        type: "POST",
        url: "/user/registro/",
        dataType: 'json',
        data: $(this).serialize(),
        success: function(response) {
            $('#msg').addClass('show');
            $('#espera').removeClass("show");
        },

        error: function() {
            console.log('Error');
        }
    });
});

$('#form_res').on("submit", function() {
    event.preventDefault();
    $('#espera-reseteo').addClass("show");
    $.ajax({
        type: "POST",
        url: "/user/resetear/",
        dataType: 'json',
        data: $(this).serialize(),
        success: function(response) {
            console.log(response);
            $('#espera-reseteo').removeClass("show");
            if (response.msg === 'ok') {
                $('#msg-envio').addClass('show');
            } else {
                $('#msg-no-mail').addClass('show');
            }
        },

        error: function() {
            console.log('Error');
        }
    });
});

$('#modalRes').on('hidden.bs.modal', function(e) {
    $('#email_res').val('');
    $('#msg-envio').removeClass('show');
    $('#msg-no-mail').removeClass('show');
});

$('#modalReg').on('hidden.bs.modal', function(e) {
    $('#txt_username').val('');
    $('#txt_pass').val('');
    $('#noCuentaReg').removeClass('show');
    $('#cuentaNoActiva').removeClass('show');
});
