//For getting CSRF token
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

jQuery(document).ready(function() {
    "use strict";
    $('.tagline').addClass('hide');
});

$('#form-reset').on("submit", function() {
    event.preventDefault();
    $('#espera-res').addClass("show");
    var csrftoken = getCookie('csrftoken');
    var contrasenia = $('#cont0').val();
    var code = $('#code').val();
    $.ajax({
        type: "POST",
        url: "/user/reset-confirm/",
        dataType: 'json',
        data: {
            contrasenia: contrasenia,
            code: code,
            csrfmiddlewaretoken: csrftoken
        },
        success: function(response) {
            console.log(response);
            $('#msg-res').addClass('show');
            $('#espera-res').removeClass("show");
        },

        error: function() {
            console.log('Error');
        }
    });
});

function validarPasswd() {
    var val0 = $('#cont0').val();
    var val1 = $('#cont1').val();
    if (val0 === val1) {
        return true;
    } else {
        $('#pass_no_igual').addClass('show');
        return false;
    }
}